package com.ssegning.sshop.keycloak.account.protocolmapper

import com.ssegning.keycloak.common.TokenRequestInterceptor
import com.ssegning.keycloak.common.TokenRequester
import com.ssegning.sshop.keycloak.account.client.handler.ApiClient
import com.ssegning.sshop.keycloak.account.constants.*
import com.ssegning.sshop.keycloak.account.service.ApiService
import com.ssegning.sshop.keycloak.account.storage.*
import org.keycloak.models.KeycloakSession
import org.keycloak.models.ProtocolMapperModel
import org.keycloak.models.RealmModel
import org.keycloak.models.UserSessionModel
import org.keycloak.protocol.oidc.mappers.*
import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.representations.IDToken
import org.keycloak.storage.StorageId
import java.util.*

open class ProviderManagerProtocolMapper : AbstractOIDCProtocolMapper(), OIDCAccessTokenMapper, OIDCIDTokenMapper,
    UserInfoTokenMapper {
    companion object {
        /*
         * A config which keycloak uses to display a generic dialog to configure the token.
         */
        private val providerConfigProperties: List<ProviderConfigProperty> = CONFIG_PROPERTIES.toMutableList()

        /*
         * The ID of the token mapper. Is public, because we need this id in our data-setup project to
         * configure the protocol mapper in keycloak.
         */
        const val PROVIDER_ID = "oidc-provider-managers-mapper"

        init {
            // The builtin protocol mapper let the user define under which claim name (key)
            // the protocol mapper writes its value. To display this option in the generic dialog
            // in keycloak, execute the following method.
            OIDCAttributeMapperHelper.addTokenClaimNameConfig(providerConfigProperties)
            // The builtin protocol mapper let the user define for which tokens the protocol mapper
            // is executed (access token, id token, user info). To add the config options for the different types
            // to the dialog execute the following method. Note that the following method uses the interfaces
            // this token mapper implements to decide which options to add to the config. So if this token
            // mapper should never be available for some sort of options, e.g. like the id token, just don't
            // implement the corresponding interface.
            OIDCAttributeMapperHelper.addIncludeInTokensConfig(
                providerConfigProperties,
                ProviderManagerProtocolMapper::class.java
            )
        }
    }

    override fun getDisplayCategory(): String = "Token Mapper"

    override fun getDisplayType(): String = "Provider Manager mapper"

    override fun getHelpText(): String = "Adds a provider claim with different providers"

    override fun getConfigProperties(): List<ProviderConfigProperty> = providerConfigProperties

    override fun getId(): String = PROVIDER_ID

    protected fun setClaim(
        token: IDToken,
        mappingModel: ProtocolMapperModel,
        userSession: UserSessionModel,
        keycloakSession: KeycloakSession
    ) {
        val api = createConfig(userSession.realm, mappingModel.config)
        val externalId = StorageId.externalId(userSession.user.id)
        val userProviders = api.userProviders(externalId)

        // adds our data to the token. Uses the parameters like the claim name which were set by the user
        // when this protocol mapper was configured in keycloak. Note that the parameters which can
        // be configured in keycloak for this protocol mapper were set in the static initializer of this class.
        //
        // Sets a static "Hello world" string, but we could write a dynamic value like a group attribute here too.
        OIDCAttributeMapperHelper.mapClaim(token, mappingModel, userProviders)
    }

    private fun createConfig(realm: RealmModel, config: Map<String, String>): ApiService {
        val apiClient = ApiClient()
        val tokenRequester = TokenRequester(
            keycloakUrl = config[SERVER_KEYCLOAK_URL]!!,
            clientId = config[SERVER_CLIENT_ID]!!,
            clientSecret = config[SERVER_CLIENT_SECRET]!!,
            scope = config[SERVER_SCOPE]!!,
            realmName = realm.name,
        )
        apiClient.basePath = config[SERVER_URL_KEY]!!
        apiClient.addAuthorization("keycloak", TokenRequestInterceptor(tokenRequester))
        return ApiService(apiClient)
    }
}
