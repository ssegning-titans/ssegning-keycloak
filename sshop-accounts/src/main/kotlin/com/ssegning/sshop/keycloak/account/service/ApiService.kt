package com.ssegning.sshop.keycloak.account.service

import com.ssegning.sshop.keycloak.account.client.api.ProviderApi
import com.ssegning.sshop.keycloak.account.client.api.UserApi
import com.ssegning.sshop.keycloak.account.client.handler.ApiClient
import com.ssegning.sshop.keycloak.account.client.model.ApiProviderManager
import com.ssegning.sshop.keycloak.account.client.model.ApiUser
import feign.FeignException
import org.keycloak.storage.UserStorageProvider
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

class ApiService(val client: ApiClient) {

    private val accountApi: UserApi = client.feignBuilder.target(UserApi::class.java, client.basePath)
    private val providerApi: ProviderApi = client.feignBuilder.target(ProviderApi::class.java, client.basePath)

    companion object {
        const val credType = "PASSWORD"
        private val log: Logger = LoggerFactory.getLogger(UserStorageProvider::class.java)
    }

    fun userProviders(externalId: String): List<ApiProviderManager> =
        providerApi.getUserProviders(UUID.fromString(externalId))

    fun getAccountById(externalId: UUID?): ApiUser? = try {
        log.debug("Get user by id $externalId")
        accountApi.getUser(externalId)
    } catch (e: FeignException) {
        log.error("Could not get a user: {}", if (e.status() == 404) "Not found" else e.message)
        if (e.status() == 404) null else throw e
    }

    fun createAccount(email: String): ApiUser? = try {
        log.debug("Create user by email $email")
        val body = ApiUser()
        body.email = email
        body.emailVerified = false
        accountApi.saveUser(body)
    } catch (e: FeignException) {
        log.error("Could not create a user: {}", if (e.status() == 404) "Not found" else e.message)
        if (e.status() == 404) null else throw e
    }

    fun getAccount(email: String): ApiUser? = try {
        log.debug("Get user by email $email")
        accountApi.getUserByEmail(email)
    } catch (e: FeignException) {
        log.error("Could not get a user: {}", if (e.status() == 404) "Not found" else e.message)
        if (e.status() == 404) null else throw e
    }

    fun getMultipleAccount(
        params: MutableMap<String, String>,
        firstResult: Int,
        maxResults: Int
    ): Collection<ApiUser> = accountApi.getUsers(
        params["query"],
        firstResult,
        maxResults,
        listOf()
    )

    fun deleteAccount(externalId: UUID) = accountApi.deleteUser(externalId)

    fun updateAccount(request: ApiUser): ApiUser =
        accountApi.saveUser(request)

    fun createPassword(externalId: String?, challengeResponse: String?) {
        val id = UUID.fromString(externalId)
        val body = mutableMapOf("challenge" to challengeResponse)
        return accountApi.saveCredential(id, credType, body)
    }

    fun disablePassword(externalId: String) = try {
        val id = UUID.fromString(externalId)
        accountApi.deleteCredential(id, credType)
    } catch (e: FeignException) {
        log.error("Could not disable user password: {}", if (e.status() == 404) "Not found" else e.message)
        if (e.status() == 404) Unit else throw e
    }

    fun validateCred(externalId: String, challengeResponse: String): Boolean {
        val id = UUID.fromString(externalId)
        val body = mutableMapOf("challenge" to challengeResponse)
        val verifyCredential = accountApi.verifyCredential(id, credType, body)
        return verifyCredential.isNotEmpty() && verifyCredential["response"] == "true"
    }

}
