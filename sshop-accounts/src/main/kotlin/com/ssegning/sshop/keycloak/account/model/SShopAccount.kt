package com.ssegning.sshop.keycloak.account.model

import com.ssegning.sshop.keycloak.account.client.model.ApiUser
import com.ssegning.sshop.keycloak.account.client.model.ApiUserImage
import com.ssegning.sshop.keycloak.account.service.ApiService
import feign.FeignException
import org.apache.commons.lang.StringUtils
import org.keycloak.common.util.MultivaluedHashMap
import org.keycloak.component.ComponentModel
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.representations.IDToken
import org.keycloak.storage.StorageId
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage
import javax.ws.rs.core.UriBuilder

class SShopAccount(
    session: KeycloakSession,
    realm: RealmModel,
    model: ComponentModel,
    private var account: ApiUser,
    private val apiService: ApiService,
) : AbstractUserAdapterFederatedStorage(session, realm, model) {

    override fun getId(): String {
        if (storageId == null) {
            storageId = StorageId(storageProviderModel.id, account.id!!.toString())
        }
        return storageId.id
    }

    override fun getAttributes(): Map<String, List<String>> {
        val map: MultivaluedHashMap<String, String> = MultivaluedHashMap<String, String>()
        map[FIRST_NAME] = listOf(account.firstName)
        map[LAST_NAME] = listOf(account.lastName)
        map[EMAIL] = listOf(account.email)
        map[USERNAME] = listOf(account.email)

        map[IDToken.PICTURE] =
            if (account.image?.url != null) listOf(imageToUrl(account.image!!))
            else listOf("https://ssegning.com/logo.png")

        map[IDToken.PHONE_NUMBER] = listOf(account.phoneNumber)
        map[IDToken.PHONE_NUMBER_VERIFIED] = listOf(if (account.phoneNumberVerified == true) "true" else "false")
        map[IDToken.LOCALE] = listOf(account.locale)

        map[ApiUser.JSON_PROPERTY_REGION] = listOf(account.region)
        return map
    }

    override fun getUsername(): String? = email

    override fun setUsername(username: String) = setEmail(username)

    override fun getFirstName(): String? = getFirstAttribute(FIRST_NAME)

    override fun setFirstName(firstName: String) = setSingleAttribute(FIRST_NAME, firstName)

    override fun getLastName(): String? = getFirstAttribute(LAST_NAME)

    override fun setLastName(lastName: String) = setSingleAttribute(LAST_NAME, lastName)

    override fun getEmail(): String? = getFirstAttribute(EMAIL)

    override fun setEmail(email: String) = setSingleAttribute(EMAIL, email.lowercase())

    override fun getFirstAttribute(name: String): String? {
        val attributes = attributes
        val values = attributes[name]
        return if (values?.isNotEmpty() == true) values[0] else null
    }

    override fun isEnabled() = true

    override fun setEnabled(enabled: Boolean) {}

    override fun isEmailVerified(): Boolean = account.emailVerified == true

    override fun setEmailVerified(verified: Boolean) = setSingleAttribute(EMAIL_VERIFIED, verified.toString())

    override fun getGroupsCount(): Long = 0

    override fun getGroupsCountByNameContaining(search: String?): Long = 0

    override fun getCreatedTimestamp(): Long = account.createdAt?.time!!

    override fun setAttribute(name: String, values: List<String>?) {
        if (!StringUtils.isEmpty(name)
            && values?.isNotEmpty() == true && !StringUtils.isEmpty(values[0])
        ) {
            val value = values[0]
            setSingleAttribute(name, value)
        }
    }

    override fun setSingleAttribute(name: String, value: String) {
        if (!StringUtils.isEmpty(value)) {
            buildChangeData(name, value)
        }
    }

    private fun buildChangeData(name: String, value: String): Boolean {
        val request = account
        when (name) {
            FIRST_NAME -> request.firstName = value
            LAST_NAME -> request.lastName = value
            EMAIL, USERNAME -> request.email = value
            EMAIL_VERIFIED -> request.emailVerified = value == "true"
            IDToken.PHONE_NUMBER -> request.phoneNumber = value
            IDToken.LOCALE -> request.locale = value
            ApiUser.JSON_PROPERTY_REGION -> request.region = value
            else -> return false
        }

        return try {
            account = apiService.updateAccount(request)
            true
        } catch (e: FeignException.FeignClientException) {
            false
        }
    }

    private fun imageToUrl(image: ApiUserImage): String {
        val uri = UriBuilder.fromUri(image.url)

        if (image.size != null) {
            uri.queryParam("s_size", image.size)
        }
        if (image.width != null) {
            uri.queryParam("s_width", image.width)
        }
        if (image.height != null) {
            uri.queryParam("s_height", image.height)
        }
        return uri.build().toString()
    }

    override fun toString(): String = "SShopAccount(id=$id, attributes=$attributes)"

}
