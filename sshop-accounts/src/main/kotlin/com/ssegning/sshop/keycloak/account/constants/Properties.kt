package com.ssegning.sshop.keycloak.account.constants

import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.provider.ProviderConfigurationBuilder

const val SERVER_CLIENT_ID = "serverClientId"
const val SERVER_CLIENT_SECRET = "serverClientSecret"
const val SERVER_URL_KEY = "serverUrl"
const val SERVER_KEYCLOAK_URL = "serverKeycloakUrl"
const val SERVER_SCOPE = "serverScope"

val CONFIG_PROPERTIES: List<ProviderConfigProperty> = ProviderConfigurationBuilder
    .create()
    .property()
    .name(SERVER_URL_KEY)
    .type(ProviderConfigProperty.STRING_TYPE)
    .label("Server Url")
    .helpText("Url where to call APIs")
    .add()
    //
    .property()
    .name(SERVER_KEYCLOAK_URL)
    .type(ProviderConfigProperty.STRING_TYPE)
    .label("Server Keycloak Url")
    .helpText("Url of the production's auth url")
    .add()
    //
    .property()
    .name(SERVER_CLIENT_ID)
    .type(ProviderConfigProperty.STRING_TYPE)
    .label("Client ID")
    .helpText("Client ID from Keycloak of the Client")
    .add()
    //
    .property()
    .name(SERVER_CLIENT_SECRET)
    .type(ProviderConfigProperty.STRING_TYPE)
    .label("Client Secret")
    .helpText("Client Secret from Keycloak of the client")
    .add()
    //
    .property()
    .name(SERVER_SCOPE)
    .type(ProviderConfigProperty.STRING_TYPE)
    .label("Scope")
    .helpText("Scope for Token request")
    .add()
    //
    .build()
