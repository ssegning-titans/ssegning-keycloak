package com.ssegning.sshop.keycloak.account.storage

import com.ssegning.keycloak.common.TokenRequestInterceptor
import com.ssegning.keycloak.common.TokenRequester
import com.ssegning.sshop.keycloak.account.client.handler.ApiClient
import com.ssegning.sshop.keycloak.account.constants.*
import com.ssegning.sshop.keycloak.account.service.ApiService
import org.apache.commons.lang.StringUtils
import org.keycloak.common.util.MultivaluedHashMap
import org.keycloak.component.ComponentModel
import org.keycloak.component.ComponentValidationException
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.provider.ServerInfoAwareProviderFactory
import org.keycloak.storage.UserStorageProviderFactory
import java.net.MalformedURLException
import java.net.URL


class AccountStorageFactory : UserStorageProviderFactory<UserStorageProvider>, ServerInfoAwareProviderFactory {

    private var apiService: ApiService? = null

    override fun create(keycloakSession: KeycloakSession, componentModel: ComponentModel): UserStorageProvider {
        if (apiService == null) {
            createConfig(keycloakSession.context.realm, componentModel.config)
        }
        return UserStorageProvider(apiService!!, keycloakSession, componentModel)
    }

    override fun getConfigProperties(): List<ProviderConfigProperty> = CONFIG_PROPERTIES.toMutableList()

    override fun getId(): String = ID

    override fun getOperationalInfo(): MutableMap<String, String> = SPI_INFO

    override fun getHelpText(): String = "SShop REST Account storage"

    @Throws(ComponentValidationException::class)
    override fun validateConfiguration(session: KeycloakSession, realm: RealmModel, model: ComponentModel) {
        val config = model.config

        val serverUrl = config.getFirst(SERVER_URL_KEY)
        if (StringUtils.isEmpty(serverUrl)) {
            throw ComponentValidationException("ServerUrl should not be empty")
        }
        try {
            URL(serverUrl)
        } catch (malformedURLException: MalformedURLException) {
            throw ComponentValidationException("ServerUrl is not a valid url")
        }

        val basicUser = config.getFirst(SERVER_CLIENT_ID)
        if (StringUtils.isEmpty(basicUser)) {
            throw ComponentValidationException("Client ID cannot be empty")
        }

        val basicPass = config.getFirst(SERVER_CLIENT_SECRET)
        if (StringUtils.isEmpty(basicPass)) {
            throw ComponentValidationException("Client Secret cannot be empty")
        }

        val keycloakUrl = config.getFirst(SERVER_KEYCLOAK_URL)
        if (StringUtils.isEmpty(keycloakUrl)) {
            throw ComponentValidationException("Keycloak's url cannot be empty")
        }
    }

    override fun onCreate(session: KeycloakSession, realm: RealmModel, model: ComponentModel) =
        createConfig(realm, model.config)

    override fun onUpdate(
        session: KeycloakSession,
        realm: RealmModel,
        oldModel: ComponentModel,
        newModel: ComponentModel
    ) = createConfig(realm, newModel.config)

    private fun createConfig(realm: RealmModel, config: MultivaluedHashMap<String, String>) {
        val apiClient = ApiClient()
        val tokenRequester = TokenRequester(
            keycloakUrl = config.getFirst(SERVER_KEYCLOAK_URL),
            clientId = config.getFirst(SERVER_CLIENT_ID),
            clientSecret = config.getFirst(SERVER_CLIENT_SECRET),
            scope = config.getFirst(SERVER_SCOPE),
            realmName = realm.name,
        )
        apiClient.basePath = config.getFirst(SERVER_URL_KEY)
        apiClient.addAuthorization("keycloak", TokenRequestInterceptor(tokenRequester))
        apiService = ApiService(apiClient)
    }

}


const val ID = "sshop-accounts-storage-provider"

private val SPI_INFO: MutableMap<String, String> =
    mutableMapOf(
        "provider_id" to ID,
        ID to "1.0.0",
        "maintainer" to "me@ssegning.com",
    )

