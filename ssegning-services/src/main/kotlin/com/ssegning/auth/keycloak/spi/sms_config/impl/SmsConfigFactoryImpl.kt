package com.ssegning.auth.keycloak.spi.sms_config.impl

import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.model.SmsSenderConfigRepresentation
import com.ssegning.auth.keycloak.model.mapToSmsSender
import com.ssegning.auth.keycloak.spi.sms_config.SmsConfigFactory
import com.ssegning.auth.keycloak.spi.sms_config.SmsConfigProvider
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderFactory
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderProvider
import org.jboss.logging.Logger
import org.keycloak.Config
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory

class SmsConfigFactoryImpl : SmsConfigFactory {

    override fun create(session: KeycloakSession): SmsConfigProvider {
        val factoriesConfigs: MutableMap<String, SmsSenderConfigRepresentation> = HashMap()

        val providers = session.getAllProviders(SmsSenderProvider::class.java)

        for (senderProvider in providers) {
            val keycloakSessionFactory = session.keycloakSessionFactory
            val providerId = senderProvider.providerId()
            val factory: SmsSenderFactory =
                keycloakSessionFactory.getProviderFactory(SmsSenderProvider::class.java, providerId) as SmsSenderFactory
            factoriesConfigs[providerId] = factory.mapToSmsSender()
        }
        return SmsConfigProviderImpl(session, factoriesConfigs)
    }

    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) = noop()

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) = noop()

    /**
     * This is called when the server shuts down.
     *
     */
    override fun close() = noop()

    override fun getId(): String = ID

    companion object {
        const val ID = "sms-config-default-impl"
        private val logger = Logger.getLogger(SmsConfigFactoryImpl::class.java)
    }
}
