package com.ssegning.auth.keycloak.credential

import com.ssegning.auth.keycloak.model.PhoneNumberCredentialModel
import org.keycloak.common.util.Time
import org.keycloak.credential.CredentialModel
import org.keycloak.credential.CredentialProvider
import org.keycloak.credential.CredentialTypeMetadata
import org.keycloak.credential.CredentialTypeMetadataContext
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.models.UserCredentialManager
import org.keycloak.models.UserModel


class PhoneNumberCredentialProvider(val session: KeycloakSession) : CredentialProvider<PhoneNumberCredentialModel> {
    override fun getType(): String = PhoneNumberCredentialModel.TYPE

    override fun createCredential(
        realm: RealmModel,
        user: UserModel,
        model: PhoneNumberCredentialModel?
    ): CredentialModel? {
        if (model !is PhoneNumberCredentialModel) {
            return null
        }

        model.type = PhoneNumberCredentialModel.TYPE
        model.createdDate = Time.currentTimeMillis()
        model.userLabel = "SMS @ " + model.phoneNumber
        model.writeCredentialData()

        session.userCredentialManager().createCredential(realm, user, model)
        return model
    }

    override fun deleteCredential(realm: RealmModel, user: UserModel, credentialId: String): Boolean {
        val userCredentialManager: UserCredentialManager = session.userCredentialManager()
        return userCredentialManager.removeStoredCredential(realm, user, credentialId)
    }

    override fun getCredentialFromModel(model: CredentialModel): PhoneNumberCredentialModel? =
        if (type != model.type) null else model as PhoneNumberCredentialModel

    override fun getCredentialTypeMetadata(metadataContext: CredentialTypeMetadataContext): CredentialTypeMetadata {
        val builder = CredentialTypeMetadata.builder()
        builder.type(type)
        builder.category(CredentialTypeMetadata.Category.TWO_FACTOR)
        builder.createAction(com.ssegning.auth.keycloak.action.UpdatePhoneNumberRequiredActionFactory.ID)
        builder.removeable(true)
        builder.displayName("phoneNumber-display-name")
        builder.helpText("phoneNumber-help-text")
        // builder.updateAction(GenerateBackupCodeAction.ID);
        // TODO configure proper FA icon for sms auth
        // builder.updateAction(GenerateBackupCodeAction.ID);
        // TODO configure proper FA icon for sms auth
        builder.iconCssClass("kcAuthenticatorMfaSmsClass")
        return builder.build(session)
    }
}
