package com.ssegning.auth.keycloak.action

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.ssegning.auth.keycloak.config.*
import org.jboss.logging.Logger
import org.keycloak.Config
import org.keycloak.authentication.RequiredActionContext
import org.keycloak.authentication.RequiredActionFactory
import org.keycloak.authentication.RequiredActionProvider
import org.keycloak.events.Details
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.models.UserModel
import org.keycloak.services.validation.Validation

class UpdatePhoneNumberRequiredActionFactory : RequiredActionProvider, RequiredActionFactory {

    override fun create(session: KeycloakSession): RequiredActionProvider = this

    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) = noop()

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) = noop()

    /**
     * This is called when the server shuts down.
     *
     */
    override fun close() = noop()

    /**
     * Called every time a user authenticates.  This checks to see if this required action should be triggered.
     * The implementation of this method is responsible for setting the required action on the UserModel.
     *
     * For example, the UpdatePassword required actions checks the password policies to see if the password has expired.
     *
     * @param context
     */
    override fun evaluateTriggers(context: RequiredActionContext) {
        if (!isUserPhoneNumberValid(context.user)) {
            context.user.addRequiredAction(ID)
        }
    }

    /**
     * If the user has a required action set, this method will be the initial call to obtain what to display to the
     * user's browser.  Return null if no action should be done.
     *
     * @param context
     * @return
     */
    override fun requiredActionChallenge(context: RequiredActionContext) {
        val authSession = context.authenticationSession

        if (isUserPhoneNumberValid(context.user)) {
            context.success()
            authSession.removeAuthNote(VALIDATE_PHONE_NUMBER_KEY)
            context.authenticationSession.removeAuthNote(VALIDATE_PHONE_NUMBER_SECRET)
            return
        }

        val phoneNumber = try {
            val tmpParsed = phoneUtil.parse(context.user.attributes[PROPERTY_PHONE_NUMBER]?.firstOrNull(), null)
            phoneUtil.format(tmpParsed, PhoneNumberUtil.PhoneNumberFormat.E164)
        } catch (e: NumberParseException) {
            null
        }

        val loginFormsProvider = context.form()

        if (phoneNumber.isNullOrEmpty()) {
            logger.debug("User has no phone number. First getting it")

            val challenge = loginFormsProvider.createForm(REQUEST_PHONE_NUMBER)
            context.challenge(challenge)
            return
        }

        logger.debug("User has phone number $phoneNumber. Then sending TAN")

        // Do not allow resending tan by simple page refresh, i.e. when tan sent,
        // it should be resent properly via email-verification endpoint
        val challenge = if (authSession.getAuthNote(VALIDATE_PHONE_NUMBER_KEY) != phoneNumber) {
            authSession.setAuthNote(VALIDATE_PHONE_NUMBER_KEY, phoneNumber)
            val event = context.event.clone().detail(Details.USERNAME, phoneNumber)
            sendSms(context.session, loginFormsProvider, event, authSession, phoneNumber)
        } else {
            loginFormsProvider.createForm(REQUEST_SMS_TAN)
        }

        context.challenge(challenge)
    }

    /**
     * Called when a required action has form input you want to process.
     *
     * @param context
     */
    override fun processAction(context: RequiredActionContext) {
        logger.debugf(
            "Resend tan for user %s",
            context.user.username
        )
        val authSession = context.authenticationSession

        val formData = context.httpRequest.decodedFormParameters

        if (formData.containsKey("resend")) {
            authSession.removeAuthNote(VALIDATE_PHONE_NUMBER_KEY)
            requiredActionChallenge(context)
            return
        }

        var phoneNumber = try {
            val tmpParsed = phoneUtil.parse(formData[PHONE_NUMBER_PROPERTY]?.firstOrNull(), null)
            phoneUtil.format(tmpParsed, PhoneNumberUtil.PhoneNumberFormat.E164)
        } catch (e: NumberParseException) {
            null
        }

        if (!phoneNumber.isNullOrEmpty()) {
            logger.debug("Phone number provided: $phoneNumber. Now sending TAN")

            val user = context.user

            user.setAttribute(PROPERTY_PHONE_NUMBER, mutableListOf(phoneNumber))
            user.setAttribute(PROPERTY_PHONE_NUMBER_VERIFIED, mutableListOf("false"))

            logger.debug("User's phone number set. Success!")

            requiredActionChallenge(context)
            return
        }

        if (!formData.containsKey(SMS_TAN_PROPERTY)) {
            requiredActionChallenge(context)
            return
        }

        phoneNumber = try {
            val tmpParsed = phoneUtil.parse(context.user.attributes[PROPERTY_PHONE_NUMBER]?.firstOrNull(), null)
            phoneUtil.format(tmpParsed, PhoneNumberUtil.PhoneNumberFormat.E164)
        } catch (e: NumberParseException) {
            null
        }

        if (Validation.isBlank(phoneNumber)) {
            context.failure()
            return
        }

        val loginFormsProvider = context.form()
        val event = context.event.clone().detail(Details.USERNAME, phoneNumber)

        val smsTan = formData[SMS_TAN_PROPERTY]?.firstOrNull()!!
        val challenge = validateTan(context.session, loginFormsProvider, event, authSession, smsTan, phoneNumber!!)
        if (challenge != null) {
            context.challenge(challenge)
            return
        }

        authSession.removeAuthNote(VALIDATE_PHONE_NUMBER_KEY)
        authSession.removeAuthNote(VALIDATE_PHONE_NUMBER_SECRET)

        val user = context.user

        user.setAttribute(PROPERTY_PHONE_NUMBER_VERIFIED, mutableListOf("true"))

        context.success()
    }

    override fun getId(): String = ID

    /**
     * Display text used in admin console to reference this required action
     *
     * @return
     */
    override fun getDisplayText(): String = "Validate phone number"

    private fun isUserPhoneNumberValid(user: UserModel): Boolean =
        user.attributes[PROPERTY_PHONE_NUMBER_VERIFIED]?.firstOrNull() == "true"

    companion object {
        const val ID = "phone-number-required-action"
        private val logger = Logger.getLogger(UpdatePhoneNumberRequiredActionFactory::class.java)
    }

}
