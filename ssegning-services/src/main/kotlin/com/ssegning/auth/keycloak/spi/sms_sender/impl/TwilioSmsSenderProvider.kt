package com.ssegning.auth.keycloak.spi.sms_sender.impl

import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.model.SmsConfigMetaData
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderProvider
import com.twilio.Twilio
import com.twilio.rest.api.v2010.account.Message
import com.twilio.type.PhoneNumber
import org.jboss.logging.Logger
import org.keycloak.models.KeycloakSession

class TwilioSmsSenderProvider(private val session: KeycloakSession) : SmsSenderProvider {

    override fun sendSmsTan(phoneNumber: String, code: String, map: SmsConfigMetaData?) {
        val accountId = map?.get(TwilioSmsSenderFactory.TWILIO_ACCOUNT_ID)?.first()!!
        val authToken = map[TwilioSmsSenderFactory.TWILIO_AUTH_TOKEN]?.first()!!
        val senderName = map[TwilioSmsSenderFactory.TWILIO_SENDER_NAME]?.first()!!

        synchronized(Twilio::class.java) {
            Twilio.init(accountId, authToken)

            try {
                val message: Message = Message.creator(
                    PhoneNumber(phoneNumber),
                    PhoneNumber(senderName),
                    "Your tan is $code"
                ).create()

                logger.debug("[Sms] -> For $accountId send by twilio ${message.messagingServiceSid} to $phoneNumber: $code")
            } catch (e: Exception) {
                logger.error("[Sms] -> Error sending code", e)
            }

            Twilio.destroy()
        }
    }

    override fun providerId(): String = TwilioSmsSenderFactory.ID

    override fun close() = noop()

    companion object {
        private val logger = Logger.getLogger(TwilioSmsSenderProvider::class.java)
    }
}
