package com.ssegning.auth.keycloak.credential

import org.keycloak.credential.CredentialProvider
import org.keycloak.credential.CredentialProviderFactory
import org.keycloak.models.KeycloakSession

class PhoneNumberCredentialProviderFactory : CredentialProviderFactory<PhoneNumberCredentialProvider> {
    override fun create(session: KeycloakSession): CredentialProvider<*> = PhoneNumberCredentialProvider(session)

    override fun getId(): String = ID

    companion object {
        const val ID = "sms-login/credential-provider"
    }
}
