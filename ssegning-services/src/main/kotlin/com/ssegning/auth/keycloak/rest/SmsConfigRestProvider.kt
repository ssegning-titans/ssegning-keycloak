package com.ssegning.auth.keycloak.rest

import com.ssegning.auth.keycloak.rest.resource.SmsConfigResource
import org.keycloak.models.KeycloakSession
import org.keycloak.services.resource.RealmResourceProvider


class SmsConfigRestProvider(session: KeycloakSession) : RealmResourceProvider {

    private val resource = SmsConfigResource(session)

    override fun close() {
    }

    /**
     *
     * Returns a JAX-RS resource instance.
     *
     * @return a JAX-RS sub-resource instance
     */
    override fun getResource(): Any = resource

}
