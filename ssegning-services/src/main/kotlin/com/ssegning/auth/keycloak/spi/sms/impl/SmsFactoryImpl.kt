package com.ssegning.auth.keycloak.spi.sms.impl

import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.spi.sms.SmsFactory
import com.ssegning.auth.keycloak.spi.sms.SmsProvider
import org.keycloak.Config
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory

class SmsFactoryImpl : SmsFactory {

    override fun create(session: KeycloakSession): SmsProvider = SmsProviderImpl(session)

    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) = noop()

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) = noop()

    /**
     * This is called when the server shuts down.
     *
     */
    override fun close() = noop()

    override fun getId(): String = ID

    companion object {
        const val ID = "sms-default-impl"
    }
}
