package com.ssegning.auth.keycloak.spi.sms_sender

import com.ssegning.auth.keycloak.model.SmsConfigMetaData
import org.keycloak.provider.Provider

interface SmsSenderProvider : Provider {

    fun sendSmsTan(phoneNumber: String, code: String, map: SmsConfigMetaData?)

    fun providerId(): String

}
