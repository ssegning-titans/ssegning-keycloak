package com.ssegning.auth.keycloak.spi.sms_config

import org.keycloak.provider.Provider
import org.keycloak.provider.ProviderFactory
import org.keycloak.provider.Spi

class SmsConfigSpi : Spi {

    override fun isInternal(): Boolean = false

    override fun getName(): String = "sms-config"

    override fun getProviderClass(): Class<out Provider> = SmsConfigProvider::class.java

    override fun getProviderFactoryClass(): Class<out ProviderFactory<out Provider>> = SmsConfigFactory::class.java
}
