package com.ssegning.auth.keycloak.spi.sms_sender

import org.keycloak.provider.Provider
import org.keycloak.provider.ProviderFactory
import org.keycloak.provider.Spi

class SmsSenderSpi : Spi {

    override fun isInternal(): Boolean = false

    override fun getName(): String = "sms-sender"

    override fun getProviderClass(): Class<out Provider> = SmsSenderProvider::class.java

    override fun getProviderFactoryClass(): Class<out ProviderFactory<out Provider>> = SmsSenderFactory::class.java
}
