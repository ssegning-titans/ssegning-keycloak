package com.ssegning.auth.keycloak.spi.sms_sender

import com.ssegning.auth.keycloak.model.SmsConfigMetaData
import org.keycloak.component.ComponentValidationException
import org.keycloak.provider.ConfiguredProvider
import org.keycloak.provider.ProviderFactory
import org.keycloak.provider.ServerInfoAwareProviderFactory

interface SmsSenderFactory : ProviderFactory<SmsSenderProvider>, ConfiguredProvider, ServerInfoAwareProviderFactory {

    @Throws(ComponentValidationException::class)
    fun validateConfig(config: SmsConfigMetaData) {
    }

    /**
     * Friendly name for the authenticator
     *
     * @return
     */
    fun getDisplayType(): String

    /**
     * Is this authenticator configurable?
     *
     * @return
     */
    fun isConfigurable(): Boolean

    /**
     * Return actual info about the provider. This info contains informations about providers configuration and operational conditions (eg. errors in connection to remote systems etc) which is
     * shown on "Server Info" page then.
     *
     * @return Map with keys describing value and relevant values itself
     */
    override fun getOperationalInfo(): MutableMap<String, String> = SPI_INFO

    companion object {
        val SPI_INFO: MutableMap<String, String> = mutableMapOf("maintainer" to "stephane.segning@ssegning.com")
    }
}
