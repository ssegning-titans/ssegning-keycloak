package com.ssegning.auth.keycloak.model

import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderFactory
import org.keycloak.provider.ProviderConfigProperty

class SmsSenderConfigRepresentation(
    val senderProviderId: String,
    val displayName: String,
    val configurable: Boolean,
    val helpText: String,
    val properties: List<ProviderConfigProperty>,
)

fun SmsSenderFactory.mapToSmsSender() = SmsSenderConfigRepresentation(
    senderProviderId = this.id,
    displayName = this.getDisplayType(),
    configurable = this.isConfigurable(),
    helpText = this.helpText,
    properties = this.configProperties
)
