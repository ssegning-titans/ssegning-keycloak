package com.ssegning.auth.keycloak.jpa.sms_config

import com.ssegning.auth.keycloak.config.noop
import org.keycloak.Config
import org.keycloak.connections.jpa.entityprovider.JpaEntityProvider
import org.keycloak.connections.jpa.entityprovider.JpaEntityProviderFactory
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory

class SmsConfigJpaEntityFactory : JpaEntityProviderFactory {
    override fun create(session: KeycloakSession): JpaEntityProvider = SmsConfigJpaEntityProvider()

    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) = noop()

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) = noop()

    /**
     * This is called when the server shuts down.
     *
     */
    override fun close() = noop()

    override fun getId(): String = ID

    companion object {
        const val ID = "sms-config-jpa-entity"
    }
}
