package com.ssegning.auth.keycloak.config

const val PROPERTY_PHONE_NUMBER = "phoneNumber"
const val PROPERTY_PHONE_NUMBER_VERIFIED = "phoneNumberVerified"
