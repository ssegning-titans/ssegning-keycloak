package com.ssegning.auth.keycloak.jpa.sms_config

import com.ssegning.auth.keycloak.converter.SmsConfigMetaDataConverter
import com.ssegning.auth.keycloak.model.SmsConfigMetaData
import javax.persistence.*

@Entity
@Table(name = "SMS_SENDER_CONFIG")
@NamedQueries(
    NamedQuery(name = "findSmsSenderConfigByRealm", query = "from SmsConfigDb where realmId = :realmId"),
    NamedQuery(
        name = "findSmsSenderConfigByRealmAndAlias",
        query = "from SmsConfigDb where realmId = :realmId and alias = :alias"
    ),
    NamedQuery(
        name = "findSmsSenderConfigByRealmAndDefault",
        query = "from SmsConfigDb where realmId = :realmId and isDefault = :isDefault"
    ),
)
@Cacheable
open class SmsConfigDb(
    @Id
    @Column(name = "ID", columnDefinition = "VARCHAR(36)")
    open var id: String? = null,

    @Column(name = "ALIAS", nullable = false)
    open var alias: String? = null,

    @Column(name = "REALM_ID", nullable = false, updatable = false)
    open var realmId: String? = null,

    @Column(name = "SENDER_PROVIDER_ID", nullable = false)
    open var senderProviderId: String? = null,

    @Column(name = "IS_DEFAULT_CONFIG", nullable = false)
    open var isDefault: Boolean = false,

    @Convert(converter = SmsConfigMetaDataConverter::class)
    @Column(name = "MAP", nullable = true, columnDefinition = "text")
    open var map: SmsConfigMetaData? = null
)
