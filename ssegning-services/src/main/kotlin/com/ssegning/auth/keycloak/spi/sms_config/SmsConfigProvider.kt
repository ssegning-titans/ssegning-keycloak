package com.ssegning.auth.keycloak.spi.sms_config

import com.ssegning.auth.keycloak.model.SmsConfigRepresentation
import com.ssegning.auth.keycloak.model.SmsSenderConfigRepresentation
import org.keycloak.provider.Provider

interface SmsConfigProvider : Provider {

    fun listConfigs(): List<SmsConfigRepresentation>

    fun addConfig(config: SmsConfigRepresentation)

    fun getSmsConfig(alias: String): SmsConfigRepresentation

    fun getDefaultSmsConfig(): SmsConfigRepresentation

    fun deleteConfig(alias: String)

    fun senderFactories(): List<SmsSenderConfigRepresentation>

    fun makeDefaultConfig(configAlias: String)

}
