package com.ssegning.auth.keycloak.model

import org.jboss.logging.Logger
import org.keycloak.credential.CredentialModel
import org.keycloak.representations.IDToken
import org.keycloak.util.JsonSerialization
import java.io.IOException


class PhoneNumberCredentialModel(credentialModel: CredentialModel?) : CredentialModel() {

    constructor() : this(null)

    var phoneNumber: String? = null

    init {
        type = TYPE
        if (credentialModel != null) {
            id = credentialModel.id
            createdDate = credentialModel.createdDate
            credentialData = credentialModel.credentialData
            secretData = credentialModel.secretData
        }
    }

    fun writeCredentialData() {
        val credentialData: MutableMap<String, String> = HashMap()
        credentialData[IDToken.PHONE_NUMBER] = phoneNumber!!
        try {
            setCredentialData(JsonSerialization.writeValueAsString(credentialData))
        } catch (e: IOException) {
            logger.errorf(e, "Could not serialize SMS credentialData")
        }
    }

    fun readCredentialData() {
        try {
            val map: Map<*, *> = JsonSerialization.readValue(
                credentialData,
                MutableMap::class.java
            )
            phoneNumber = map[IDToken.PHONE_NUMBER] as String?
        } catch (e: IOException) {
            logger.errorf(e, "Could not deserialize SMS Credential data")
        }
    }

    companion object {
        const val TYPE = "phoneNumber"
        private val logger = Logger.getLogger(PhoneNumberCredentialModel::class.java)
    }
}
