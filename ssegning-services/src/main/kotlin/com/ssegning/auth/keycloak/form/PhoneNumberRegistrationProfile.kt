package com.ssegning.auth.keycloak.form

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.ssegning.auth.keycloak.config.phoneUtil
import org.jboss.logging.Logger
import org.keycloak.Config
import org.keycloak.authentication.FormAction
import org.keycloak.authentication.FormActionFactory
import org.keycloak.authentication.FormContext
import org.keycloak.authentication.ValidationContext
import org.keycloak.forms.login.LoginFormsProvider
import org.keycloak.models.AuthenticationExecutionModel.Requirement
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.models.RealmModel
import org.keycloak.models.UserModel
import org.keycloak.models.utils.FormMessage
import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.services.validation.Validation

class PhoneNumberRegistrationProfile : FormAction, FormActionFactory {

    override fun create(session: KeycloakSession): FormAction = this

    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) {}

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) {
    }

    override fun close() {
    }

    override fun getId() = ID

    override fun getHelpText() = "Validate phone number and add it to user attributes"

    override fun getConfigProperties(): MutableList<ProviderConfigProperty>? = null

    /**
     * Friendly name for the authenticator
     *
     * @return
     */
    override fun getDisplayType(): String = "Phone number validation"

    /**
     * General authenticator type, i.e. totp, password, cert.
     *
     * @return null if not a referencable category
     */
    override fun getReferenceCategory(): String? = null

    /**
     * Is this authenticator configurable?
     *
     * @return
     */
    override fun isConfigurable(): Boolean = false

    /**
     * What requirement settings are allowed.
     *
     * @return
     */
    override fun getRequirementChoices() = REQUIREMENT_CHOICES

    /**
     *
     * Does this authenticator have required actions that can set if the user does not have
     * this authenticator set up?
     *
     *
     * @return
     */
    override fun isUserSetupAllowed(): Boolean = false

    /**
     * When a FormAuthenticator is rendering the challenge page, even FormAction.buildPage() method will be called
     * This gives the FormAction the opportunity to add additional attributes to the form to be displayed.
     *
     * @param context
     * @param form
     */
    override fun buildPage(context: FormContext, form: LoginFormsProvider) {
        form.setAttribute("usernameRequired", true)
    }

    /**
     * This is the first phase of form processing.  Each FormAction.validate() method is called.  This gives the
     * FormAction a chance to validate and challenge if user input is invalid.
     *
     * @param context
     */
    override fun validate(context: ValidationContext) {
        val formData = context.httpRequest.decodedFormParameters
        val errors: MutableList<FormMessage> = ArrayList()
        context.event.detail("register_method", "form")
        val phoneNumber = formData.getFirst("username")

        if (Validation.isBlank(phoneNumber)) {
            errors.add(FormMessage("username", "missingPhoneNumberMessage"))
        }

        try {
            val parsed = phoneUtil.parse(phoneNumber, null)
            if (!phoneUtil.isValidNumber(parsed)) {
                errors.add(FormMessage("username", "invalidPhoneNumberMessage"))
                logger.debugf("Invalid phone number %s", phoneNumber)
            }
        } catch (err: NumberParseException) {
            logger.error("Invalid phone number", err)
            errors.add(FormMessage("username", err.message, *err.stackTrace))
        }

        if (errors.size > 0) {
            context.error("invalid_registration")
            context.validationError(formData, errors)
        } else {
            context.success()
        }
    }

    /**
     * Called after all validate() calls of all FormAction providers are successful.
     *
     * @param context
     */
    override fun success(context: FormContext) {
        val user = context.user
        val formData = context.httpRequest.decodedFormParameters

        val phoneNumberUnformatted = formData.getFirst("username")
        val phoneNumber = phoneUtil.parse(phoneNumberUnformatted, null)
        val phoneNumberFormattedTextField = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164)

        logger.debug("Correct phone number for ${user.username} is $phoneNumberFormattedTextField")

        user.username = phoneNumberFormattedTextField
    }

    /**
     * Does this FormAction require that a user be set? For registration, this method will always return false.
     *
     * @return
     */
    override fun requiresUser(): Boolean = false

    /**
     * Is this FormAction configured for the current user?
     *
     * @param session
     * @param realm
     * @param user
     * @return
     */
    override fun configuredFor(session: KeycloakSession, realm: RealmModel, user: UserModel) = true

    /**
     * Set actions to configure authenticator
     *
     */
    override fun setRequiredActions(session: KeycloakSession, realm: RealmModel, user: UserModel) =
        user.addRequiredAction(com.ssegning.auth.keycloak.action.UpdatePhoneNumberRequiredActionFactory.ID)

    companion object {
        const val ID = "phone-number-registration-profile"

        private val REQUIREMENT_CHOICES = arrayOf(
            Requirement.REQUIRED,
            Requirement.DISABLED
        )
        private val logger = Logger.getLogger(PhoneNumberRegistrationProfile::class.java)
    }
}

