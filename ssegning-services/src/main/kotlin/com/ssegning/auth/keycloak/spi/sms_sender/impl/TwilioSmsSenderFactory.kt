package com.ssegning.auth.keycloak.spi.sms_sender.impl

import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.model.SmsConfigMetaData
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderFactory
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderProvider
import org.keycloak.Config
import org.keycloak.component.ComponentValidationException
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.provider.ProviderConfigurationBuilder

class TwilioSmsSenderFactory : SmsSenderFactory {

    @Throws(ComponentValidationException::class)
    override fun validateConfig(config: SmsConfigMetaData) {
        val accountId = config[TWILIO_ACCOUNT_ID]?.first()
        val authToken = config[TWILIO_AUTH_TOKEN]?.first()
        val senderName = config[TWILIO_SENDER_NAME]?.first()

        if (accountId.isNullOrEmpty() || authToken.isNullOrEmpty() || senderName.isNullOrEmpty()) {
            throw ComponentValidationException()
        }
    }

    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) = noop()

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) = noop()

    /**
     * This is called when the server shuts down.
     *
     */
    override fun close() = noop()

    override fun getId(): String = ID

    override fun order(): Int = 100

    /**
     * Friendly name for the authenticator
     *
     * @return
     */
    override fun getDisplayType(): String = "Twilio SMS Sender"

    /**
     * Is this authenticator configurable?
     *
     * @return
     */
    override fun isConfigurable(): Boolean = true

    override fun create(session: KeycloakSession): SmsSenderProvider = TwilioSmsSenderProvider(session)

    override fun getHelpText(): String = "Twilio SMS OTP Sender"

    override fun getConfigProperties(): MutableList<ProviderConfigProperty> = CONFIG_PROPERTIES.toMutableList()

    companion object {
        const val ID = "twilio-sms-sender"

        const val TWILIO_ACCOUNT_ID = "twilio-account-id"
        const val TWILIO_AUTH_TOKEN = "twilio-auth-token"
        const val TWILIO_SENDER_NAME = "twilio-sender-name"

        val CONFIG_PROPERTIES: List<ProviderConfigProperty> = ProviderConfigurationBuilder
            .create()
            .property()
            .name(TWILIO_ACCOUNT_ID)
            .type(ProviderConfigProperty.STRING_TYPE)
            .label("Account SID")
            .helpText("Twilio's account SID")
            .add()
            //
            .property()
            .name(TWILIO_AUTH_TOKEN)
            .type(ProviderConfigProperty.STRING_TYPE)
            .label("Auth token")
            .helpText("Twilio's auth token")
            .add()
            //
            .property()
            .name(TWILIO_SENDER_NAME)
            .type(ProviderConfigProperty.STRING_TYPE)
            .label("Sender name")
            .helpText("Twilio's Sender name")
            .add()
            //
            .build()
    }
}


