package com.ssegning.auth.keycloak.converter

import com.fasterxml.jackson.databind.ObjectMapper
import com.ssegning.auth.keycloak.model.SmsConfigMetaData
import javax.persistence.AttributeConverter

class SmsConfigMetaDataConverter : AttributeConverter<SmsConfigMetaData, String> {

    private val objectMapper = ObjectMapper()

    /**
     * Converts the value stored in the entity attribute into the
     * data representation to be stored in the database.
     *
     * @param attribute  the entity attribute value to be converted
     * @return  the converted data to be stored in the database
     * column
     */
    override fun convertToDatabaseColumn(attribute: SmsConfigMetaData?): String? {
        return if (attribute != null) {
            try {
                objectMapper.writeValueAsString(attribute)
            } catch (e: Exception) {
                return null
            }
        } else {
            null
        }
    }

    /**
     * Converts the data stored in the database column into the
     * value to be stored in the entity attribute.
     * Note that it is the responsibility of the converter writer to
     * specify the correct `dbData` type for the corresponding
     * column for use by the JDBC driver: i.e., persistence providers are
     * not expected to do such type conversion.
     *
     * @param dbData  the data from the database column to be
     * converted
     * @return  the converted value to be stored in the entity
     * attribute
     */
    override fun convertToEntityAttribute(dbData: String?): SmsConfigMetaData? {
        return if (dbData != null) {
            try {
                objectMapper.readValue(dbData, SmsConfigMetaData::class.java)
            } catch (e: Exception) {
                return null
            }
        } else {
            null
        }
    }
}
