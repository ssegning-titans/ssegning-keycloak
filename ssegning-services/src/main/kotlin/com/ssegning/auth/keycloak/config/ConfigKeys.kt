package com.ssegning.auth.keycloak.config

const val REQUEST_SMS_TAN = "request-sms-tan.ftl"
const val REQUEST_PHONE_NUMBER = "request-phone-number.ftl"

const val VALIDATE_PHONE_NUMBER_KEY = "VALIDATE_PHONE_NUMBER_KEY"

const val VALIDATE_PHONE_NUMBER_SECRET = "VALIDATE_PHONE_NUMBER_SECRET"
const val PHONE_NUMBER_PROVIDED_COOKIE_KEY = "PHONE_NUMBER_PROVIDED_COOKIE"
const val SMS_TAN_PROPERTY = "smsTan"
const val PHONE_NUMBER_PROPERTY = "phoneNumber"
