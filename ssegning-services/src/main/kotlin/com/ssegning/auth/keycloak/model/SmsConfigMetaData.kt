package com.ssegning.auth.keycloak.model

import org.keycloak.common.util.MultivaluedHashMap

class SmsConfigMetaData : MultivaluedHashMap<String, String>()
