package com.ssegning.auth.keycloak.spi.sms_sender.impl

import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.model.SmsConfigMetaData
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderProvider
import org.jboss.logging.Logger

class LoggerSmsSenderProvider : SmsSenderProvider {

    override fun sendSmsTan(phoneNumber: String, code: String, map: SmsConfigMetaData?) {
        logger.info("Should send $code to $phoneNumber")
    }

    override fun providerId(): String = LoggerSmsSenderFactory.ID

    override fun close() = noop()

    companion object {
        private val logger = Logger.getLogger(LoggerSmsSenderProvider::class.java)
    }
}
