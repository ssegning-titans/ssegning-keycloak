package com.ssegning.auth.keycloak.spi.sms

import org.keycloak.provider.Provider
import org.keycloak.provider.ProviderFactory
import org.keycloak.provider.Spi

class SmsSpi : Spi {

    override fun isInternal(): Boolean = false

    override fun getName(): String = "sms"

    override fun getProviderClass(): Class<out Provider> = SmsProvider::class.java

    override fun getProviderFactoryClass(): Class<out ProviderFactory<out Provider>> = SmsFactory::class.java
}
