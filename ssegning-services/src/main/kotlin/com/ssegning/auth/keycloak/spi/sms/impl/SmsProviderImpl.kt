package com.ssegning.auth.keycloak.spi.sms.impl

import com.ssegning.auth.keycloak.config.generateTan
import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.model.SmsConfigRepresentation
import com.ssegning.auth.keycloak.model.SmsResponse
import com.ssegning.auth.keycloak.spi.sms.SmsProvider
import com.ssegning.auth.keycloak.spi.sms_config.SmsConfigProvider
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderProvider
import org.keycloak.models.KeycloakSession
import org.keycloak.models.utils.SHAPasswordEncoder

class SmsProviderImpl(private val session: KeycloakSession) : SmsProvider {

    override fun generateAndSendSmsTan(phoneNumber: String): SmsResponse {
        val code = generateTan(6)
        val secret = encode(code + phoneNumber)

        val smsConfig = getSmsDefaultConfig()
        val smsSenderProvider = session.getProvider(SmsSenderProvider::class.java, smsConfig.senderProviderId)
        smsSenderProvider.sendSmsTan(phoneNumber, "T-$code", smsConfig.map)

        return SmsResponse(secret)
    }

    override fun validateSmsTan(phoneNumber: String, secret: String, code: String): Boolean =
        verify(code + phoneNumber, secret)

    private fun verify(raw: String, secret: String): Boolean = passwordEncoder.verify(raw, secret)

    private fun encode(string: String): String = passwordEncoder.encode(string)

    override fun providerId(): String = SmsFactoryImpl.ID

    override fun close() = noop()

    private fun getSmsDefaultConfig(): SmsConfigRepresentation = getService().getDefaultSmsConfig()

    private fun getService(): SmsConfigProvider = session.getProvider(SmsConfigProvider::class.java)

    companion object {
        val passwordEncoder = SHAPasswordEncoder(256)
    }

}
