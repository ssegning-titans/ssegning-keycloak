package com.ssegning.auth.keycloak.model

import com.ssegning.auth.keycloak.jpa.sms_config.SmsConfigDb

class SmsConfigRepresentation(
    var senderProviderId: String? = null,
    var alias: String? = null,
    var isDefault: Boolean? = null,
    var map: SmsConfigMetaData? = null,
) {
    constructor(entity: SmsConfigDb) : this(
        entity.senderProviderId!!, entity.alias!!, entity.isDefault,
        entity.map
    )
}
