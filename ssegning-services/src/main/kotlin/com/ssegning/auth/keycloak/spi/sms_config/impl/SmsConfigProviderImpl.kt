package com.ssegning.auth.keycloak.spi.sms_config.impl

import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.jpa.sms_config.SmsConfigDb
import com.ssegning.auth.keycloak.model.SmsConfigRepresentation
import com.ssegning.auth.keycloak.model.SmsSenderConfigRepresentation
import com.ssegning.auth.keycloak.spi.sms_config.SmsConfigProvider
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderFactory
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderProvider
import org.keycloak.connections.jpa.JpaConnectionProvider
import org.keycloak.models.KeycloakSession
import org.keycloak.models.utils.KeycloakModelUtils
import java.util.*
import javax.persistence.NoResultException


class SmsConfigProviderImpl(
    private val session: KeycloakSession,
    private val factoriesConfigs: Map<String, SmsSenderConfigRepresentation>,
) : SmsConfigProvider {

    init {
        if (session.context.realm == null) {
            throw IllegalStateException("The service cannot accept a session without a realm in its context.")
        }
    }

    override fun listConfigs(): List<SmsConfigRepresentation> {
        val entities: List<SmsConfigDb> = getEntityManager().createNamedQuery(
            "findSmsSenderConfigByRealm",
            SmsConfigDb::class.java
        )
            .setParameter("realmId", realm().id)
            .resultList

        val result: MutableList<SmsConfigRepresentation> = LinkedList()
        for (entity in entities) {
            result.add(SmsConfigRepresentation(entity))
        }
        return result
    }

    override fun addConfig(config: SmsConfigRepresentation) {
        val entity: SmsConfigDb = try {
            getEntityManager()
                .createNamedQuery(
                    "findSmsSenderConfigByRealmAndAlias",
                    SmsConfigDb::class.java
                )
                .setParameter("realmId", realm().id)
                .setParameter("alias", config.alias)
                .singleResult
        } catch (e: NoResultException) {
            val tmp = SmsConfigDb()
            tmp.id = KeycloakModelUtils.generateId()
            tmp
        }

        val factory: SmsSenderFactory =
            session.keycloakSessionFactory.getProviderFactory(
                SmsSenderProvider::class.java,
                config.senderProviderId!!
            ) as SmsSenderFactory

        config.map?.let { factory.validateConfig(it) }

        entity.alias = config.alias!!
        entity.realmId = realm().id
        entity.senderProviderId = config.senderProviderId!!
        entity.isDefault = config.isDefault == true
        entity.map = config.map

        getEntityManager().persist(entity)
    }

    override fun getSmsConfig(alias: String): SmsConfigRepresentation {
        val entity: SmsConfigDb = getEntityManager().createNamedQuery(
            "findSmsSenderConfigByRealmAndAlias",
            SmsConfigDb::class.java
        )
            .setParameter("realmId", realm().id)
            .setParameter("alias", alias)
            .singleResult!!
        return SmsConfigRepresentation(entity)
    }

    override fun getDefaultSmsConfig(): SmsConfigRepresentation {
        val entity: SmsConfigDb = getEntityManager().createNamedQuery(
            "findSmsSenderConfigByRealmAndDefault",
            SmsConfigDb::class.java
        )
            .setParameter("realmId", realm().id)
            .setParameter("isDefault", true)
            .singleResult!!
        return SmsConfigRepresentation(entity)
    }

    override fun deleteConfig(alias: String) {
        val entity: SmsConfigDb = getEntityManager().createNamedQuery(
            "findSmsSenderConfigByRealmAndAlias",
            SmsConfigDb::class.java
        )
            .setParameter("realmId", realm().id)
            .setParameter("alias", alias)
            .singleResult!!

        getEntityManager().remove(entity)
    }

    override fun close() = noop()

    override fun senderFactories() = factoriesConfigs.values.toList()

    override fun makeDefaultConfig(configAlias: String) {
        val entities: List<SmsConfigDb> = getEntityManager().createNamedQuery(
            "findSmsSenderConfigByRealm",
            SmsConfigDb::class.java
        )
            .setParameter("realmId", realm().id)
            .resultList

        for (entity in entities) {
            if (entity.alias == configAlias) {
                entity.isDefault = true
                getEntityManager().persist(entity)
            } else {
                if (entity.isDefault) {
                    entity.isDefault = false
                    getEntityManager().persist(entity)
                }
            }
        }
    }

    private fun realm() = session.context.realm

    private fun getEntityManager() = session.getProvider(JpaConnectionProvider::class.java).entityManager
}
