package com.ssegning.auth.keycloak.rest

import com.ssegning.auth.keycloak.config.noop
import org.keycloak.Config
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.services.resource.RealmResourceProvider
import org.keycloak.services.resource.RealmResourceProviderFactory


class SmsConfigRestProviderFactory : RealmResourceProviderFactory {

    var session: KeycloakSession? = null

    override fun create(session: KeycloakSession): RealmResourceProvider = SmsConfigRestProvider(session)

    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) = noop()

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) = noop()

    override fun close() = noop()

    override fun getId(): String = ID

    companion object {
        const val ID = "sms-config"
    }
}
