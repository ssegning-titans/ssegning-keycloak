package com.ssegning.auth.keycloak.rest.resource

import com.ssegning.auth.keycloak.model.SmsConfigRepresentation
import com.ssegning.auth.keycloak.model.SmsSenderConfigRepresentation
import com.ssegning.auth.keycloak.spi.sms_config.SmsConfigProvider
import org.jboss.resteasy.annotations.cache.NoCache
import org.keycloak.models.KeycloakSession
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response


class SmsConfigResource(private val session: KeycloakSession) {

    @GET
    @Path("")
    @NoCache
    @Produces(MediaType.APPLICATION_JSON)
    fun getSmsConfig(): List<SmsConfigRepresentation> {
        checkRealmAdmin()
        return getService().listConfigs()
    }

    @POST
    @Path("")
    @NoCache
    @Consumes(MediaType.APPLICATION_JSON)
    fun createSmsConfig(rep: SmsConfigRepresentation): Response {
        checkRealmAdmin()
        getService().addConfig(rep)
        return Response.created(session.context.uri.absolutePathBuilder.path(rep.alias).build()).build()
    }

    @PUT
    @Path("{config-alias}")
    @NoCache
    @Consumes(MediaType.APPLICATION_JSON)
    fun makeDefaultConfig(@PathParam("config-alias") configAlias: String): Response {
        checkRealmAdmin()
        getService().makeDefaultConfig(configAlias)
        return Response.noContent().build()
    }

    @GET
    @NoCache
    @Path("{config-alias}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getSmsConfig(@PathParam("config-alias") configAlias: String): SmsConfigRepresentation {
        checkRealmAdmin()
        return getService().getSmsConfig(configAlias)
    }

    @DELETE
    @NoCache
    @Path("{config-alias}")
    @Produces(MediaType.APPLICATION_JSON)
    fun deleteSmsConfig(@PathParam("config-alias") configAlias: String): Response {
        checkRealmAdmin()
        getService().deleteConfig(configAlias)
        return Response.noContent().build()
    }

    @GET
    @NoCache
    @Path("sender-types")
    @Produces(MediaType.APPLICATION_JSON)
    fun getSenderTypes(): List<SmsSenderConfigRepresentation> {
        checkRealmAdmin()
        return getService().senderFactories()
    }

    private fun getService(): SmsConfigProvider = session.getProvider(SmsConfigProvider::class.java)

    private fun checkRealmAdmin() {
        // TODO
    }
}
