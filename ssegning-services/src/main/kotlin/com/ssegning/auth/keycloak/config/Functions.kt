package com.ssegning.auth.keycloak.config

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.ssegning.auth.keycloak.spi.sms.SmsProvider
import org.jboss.logging.Logger
import org.jboss.resteasy.spi.HttpResponse
import org.keycloak.authentication.AuthenticationFlowContext
import org.keycloak.common.util.ServerCookie
import org.keycloak.events.EventBuilder
import org.keycloak.forms.login.LoginFormsProvider
import org.keycloak.models.KeycloakSession
import org.keycloak.sessions.AuthenticationSessionModel
import java.net.URI
import javax.ws.rs.core.Cookie
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.Response

private val logger = Logger.getLogger("Functions")

val phoneUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()

fun noop() {
    /* no-op */
}

fun makeCookieKey(phoneNumber: String) = "$PHONE_NUMBER_PROVIDED_COOKIE_KEY-$phoneNumber"

fun setCookie(context: AuthenticationFlowContext, phoneNumber: String) {
    val config = context.authenticatorConfig
    var maxCookieAge = 60 * 60 * 24 * 30 // 30 days
    if (config != null) {
        // maxCookieAge = Integer.valueOf(config.config["cookie.max.age"])
        maxCookieAge = (60 * 60 * 24) * 4 // 4 Days
    }

    val uri: URI = context.uriInfo.baseUriBuilder.path("realms").path(context.realm.name).build()

    addCookie(
        context, phoneNumber, "true",
        uri.rawPath,
        null, null,
        maxCookieAge,
        secure = false, httpOnly = true
    )
}

fun hasCookie(context: AuthenticationFlowContext, phoneNumber: String): Boolean {
    val cookie: Cookie? = context.httpRequest.httpHeaders.cookies[makeCookieKey(phoneNumber)]
    val result = cookie != null
    if (result) {
        logger.debug("Bypassing secret question because cookie is set")
    }
    return result
}

fun addCookie(
    context: AuthenticationFlowContext,
    phoneNumber: String,
    value: String,
    path: String,
    domain: String?,
    comment: String?,
    maxAge: Int,
    secure: Boolean,
    httpOnly: Boolean
) {
    val response: HttpResponse = context.session.context.getContextObject(HttpResponse::class.java)

    val cookieBuf = StringBuffer()
    ServerCookie.appendCookieValue(
        cookieBuf,
        1,
        makeCookieKey(phoneNumber),
        value,
        path,
        domain,
        comment,
        maxAge,
        secure,
        httpOnly,
        null
    )

    val cookie = cookieBuf.toString()
    response.outputHeaders.add(HttpHeaders.SET_COOKIE, cookie)
}


fun validateTan(
    session: KeycloakSession,
    forms: LoginFormsProvider,
    event: EventBuilder,
    authSession: AuthenticationSessionModel,
    code: String,
    phoneNumber: String
): Response? {
    try {
        val secret = authSession.getAuthNote(VALIDATE_PHONE_NUMBER_SECRET)
        val status = session
            .getProvider(SmsProvider::class.java)
            .validateSmsTan(phoneNumber, secret, code)
        if (!status) {
            logger.error("Wrong tan provided for $phoneNumber : $code")
            event.error("wrongTanProvided")
            return forms
                .setAttribute("phoneNumber", phoneNumber)
                .createForm(REQUEST_SMS_TAN)
        }
        event.success()

        return null
    } catch (e: Exception) {
        logger.error("Failed to validate sms tan", e)
        event.error("tanValidationErrorMessage")
        return forms.createForm(REQUEST_SMS_TAN)
    }
}

fun sendSms(
    session: KeycloakSession,
    forms: LoginFormsProvider,
    event: EventBuilder,
    authSession: AuthenticationSessionModel,
    phoneNumber: String
): Response {
    try {
        val secretResponse = session
            .getProvider(SmsProvider::class.java)
            .generateAndSendSmsTan(phoneNumber)
        authSession.setAuthNote(VALIDATE_PHONE_NUMBER_SECRET, secretResponse.secret)
        event.success()
    } catch (e: Exception) {
        logger.error("Failed to send otp per sms", e)
        event.error("smsTanErrorMessage")
    }
    return forms
        .setAttribute("phoneNumber", phoneNumber)
        .createForm(REQUEST_SMS_TAN)
}

private val numbers = ('0'..'9')

fun generateTan(length: Int): String = (1..length)
    .map { numbers.random() }
    .joinToString("")
