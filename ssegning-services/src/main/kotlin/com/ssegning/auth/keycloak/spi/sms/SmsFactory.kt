package com.ssegning.auth.keycloak.spi.sms

import org.keycloak.provider.ProviderFactory
import org.keycloak.provider.ServerInfoAwareProviderFactory

interface SmsFactory : ProviderFactory<SmsProvider>, ServerInfoAwareProviderFactory {

    /**
     * Return actual info about the provider. This info contains informations about providers configuration and operational conditions (eg. errors in connection to remote systems etc) which is
     * shown on "Server Info" page then.
     *
     * @return Map with keys describing value and relevant values itself
     */
    override fun getOperationalInfo(): MutableMap<String, String> = SPI_INFO

    companion object {
        val SPI_INFO: MutableMap<String, String> = mutableMapOf("maintainer" to "stephane.segning@ssegning.com")
    }
}
