package com.ssegning.auth.keycloak.spi.sms

import com.ssegning.auth.keycloak.model.SmsResponse
import org.keycloak.provider.Provider

interface SmsProvider : Provider {

    fun generateAndSendSmsTan(phoneNumber: String): SmsResponse

    fun validateSmsTan(phoneNumber: String, secret: String, code: String): Boolean

    fun providerId(): String

}
