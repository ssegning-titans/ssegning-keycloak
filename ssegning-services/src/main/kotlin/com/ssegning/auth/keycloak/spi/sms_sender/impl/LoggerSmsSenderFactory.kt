package com.ssegning.auth.keycloak.spi.sms_sender.impl

import com.ssegning.auth.keycloak.config.noop
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderFactory
import com.ssegning.auth.keycloak.spi.sms_sender.SmsSenderProvider
import org.keycloak.Config
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.provider.ProviderConfigProperty

class LoggerSmsSenderFactory : SmsSenderFactory {
    /**
     * Only called once when the factory is first created.  This config is pulled from keycloak_server.json
     *
     * @param config
     */
    override fun init(config: Config.Scope) = noop()

    /**
     * Called after all provider factories have been initialized
     */
    override fun postInit(factory: KeycloakSessionFactory) = noop()

    /**
     * This is called when the server shuts down.
     *
     */
    override fun close() = noop()

    override fun getId(): String = ID

    override fun order(): Int = 100

    /**
     * Friendly name for the authenticator
     *
     * @return
     */
    override fun getDisplayType(): String = "Simple Logger - No sender"

    /**
     * Is this authenticator configurable?
     *
     * @return
     */
    override fun isConfigurable(): Boolean = false

    override fun create(session: KeycloakSession): SmsSenderProvider = LoggerSmsSenderProvider()

    override fun getHelpText(): String =
        "Log code and phone number on the console. This should never ever be used in production, as it's basically printing username/password on the console. THIS IS FOR DEV PURPOSE ONLY"

    override fun getConfigProperties() = mutableListOf<ProviderConfigProperty>()

    companion object {
        const val ID = "logger-sms-sender"
    }
}


