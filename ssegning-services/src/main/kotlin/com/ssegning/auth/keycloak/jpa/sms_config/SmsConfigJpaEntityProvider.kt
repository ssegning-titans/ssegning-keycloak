package com.ssegning.auth.keycloak.jpa.sms_config

import com.ssegning.auth.keycloak.config.noop
import org.keycloak.connections.jpa.entityprovider.JpaEntityProvider

class SmsConfigJpaEntityProvider : JpaEntityProvider {

    override fun close() = noop()

    override fun getEntities(): MutableList<Class<*>> = mutableListOf(SmsConfigDb::class.java)

    override fun getChangelogLocation(): String = "META-INF/sms-config-changelog.xml"

    override fun getFactoryId(): String = SmsConfigJpaEntityFactory.ID
}
