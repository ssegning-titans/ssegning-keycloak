package com.ssegning.auth.keycloak.authenticator

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.ssegning.auth.keycloak.action.UpdatePhoneNumberRequiredActionFactory
import com.ssegning.auth.keycloak.config.*
import org.jboss.logging.Logger
import org.keycloak.Config
import org.keycloak.authentication.AuthenticationFlowContext
import org.keycloak.authentication.Authenticator
import org.keycloak.authentication.AuthenticatorFactory
import org.keycloak.events.Details
import org.keycloak.models.AuthenticationExecutionModel.Requirement
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.models.RealmModel
import org.keycloak.models.UserModel
import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.provider.ServerInfoAwareProviderFactory
import org.keycloak.services.validation.Validation

class ValidatePhoneNumberAuthenticator() : Authenticator, AuthenticatorFactory, ServerInfoAwareProviderFactory {

    companion object {
        const val ID = "sms-login/tan-verification"
        val REQUIREMENT_CHOICES = arrayOf(
            Requirement.REQUIRED,
            Requirement.ALTERNATIVE,
            Requirement.CONDITIONAL,
            Requirement.DISABLED,
        )

        private val infos: Map<String, String> = mutableMapOf("version" to "3.0.0")
        private val logger = Logger.getLogger(ValidatePhoneNumberAuthenticator::class.java)
    }

    override fun create(session: KeycloakSession): Authenticator = this

    override fun init(config: Config.Scope) = noop()

    override fun postInit(factory: KeycloakSessionFactory) = noop()

    override fun close() = noop()

    override fun getId(): String = ID

    override fun getHelpText(): String = "Validate phone number for 2FA"

    override fun getConfigProperties(): MutableList<ProviderConfigProperty>? = null

    override fun getDisplayType(): String = "Validate phone number"

    override fun getReferenceCategory(): String? = null

    override fun isConfigurable(): Boolean = false

    override fun getRequirementChoices(): Array<Requirement> = REQUIREMENT_CHOICES

    override fun isUserSetupAllowed(): Boolean = true

    override fun authenticate(context: AuthenticationFlowContext) {
        val phoneNumber = try {
            val tmpParsed = phoneUtil.parse(context.user.attributes[PROPERTY_PHONE_NUMBER]?.firstOrNull(), null)
            phoneUtil.format(tmpParsed, PhoneNumberUtil.PhoneNumberFormat.E164)
        } catch (e: NumberParseException) {
            null
        }

        if (hasCookie(context, phoneNumber!!)) {
            context.success()
            return
        }

        val loginFormsProvider = context.form()

        val authSession = context.authenticationSession

        // Do not allow resending e-mail by simple page refresh, i.e. when e-mail sent,
        // it should be resent properly via email-verification endpoint
        val challenge = if (authSession.getAuthNote(VALIDATE_PHONE_NUMBER_KEY) != phoneNumber) {
            authSession.setAuthNote(VALIDATE_PHONE_NUMBER_KEY, phoneNumber)
            val event = context.event.clone().detail(Details.USERNAME, phoneNumber)
            sendSms(context.session, loginFormsProvider, event, authSession, phoneNumber)
        } else {
            loginFormsProvider.createForm(REQUEST_SMS_TAN)
        }

        context.challenge(challenge)
    }

    override fun action(context: AuthenticationFlowContext) {
        val authSession = context.authenticationSession

        val formData = context.httpRequest.decodedFormParameters
        if (formData.containsKey("resend")) {
            authSession.removeAuthNote(VALIDATE_PHONE_NUMBER_KEY)
            authenticate(context)
            return
        }

        if (!formData.containsKey(SMS_TAN_PROPERTY)) {
            authenticate(context)
            return
        }

        val phoneNumber = try {
            val tmpParsed = phoneUtil.parse(context.user.attributes[PROPERTY_PHONE_NUMBER]?.firstOrNull(), null)
            phoneUtil.format(tmpParsed, PhoneNumberUtil.PhoneNumberFormat.E164)
        } catch (e: NumberParseException) {
            null
        }

        if (Validation.isBlank(phoneNumber)) {
            context.attempted()
            return
        }

        val loginFormsProvider = context.form()
        val event = context.event.clone().detail(Details.USERNAME, phoneNumber)

        val smsTan = formData[SMS_TAN_PROPERTY]?.firstOrNull()!!
        val challenge = validateTan(context.session, loginFormsProvider, event, authSession, smsTan, phoneNumber!!)
        if (challenge != null) {
            context.challenge(challenge)
            return
        }

        authSession.removeAuthNote(VALIDATE_PHONE_NUMBER_KEY)
        authSession.removeAuthNote(VALIDATE_PHONE_NUMBER_SECRET)

        val user = context.user

        user.setAttribute(PROPERTY_PHONE_NUMBER_VERIFIED, mutableListOf("true"))

        setCookie(context, phoneNumber)
        context.success()
    }

    override fun requiresUser(): Boolean = true

    override fun configuredFor(session: KeycloakSession, realm: RealmModel, user: UserModel): Boolean =
        user.attributes[PROPERTY_PHONE_NUMBER_VERIFIED]?.firstOrNull() == "true"

    override fun setRequiredActions(session: KeycloakSession, realm: RealmModel, user: UserModel) =
        user.addRequiredAction(UpdatePhoneNumberRequiredActionFactory.ID)

    override fun getOperationalInfo(): Map<String, String> = infos

}
