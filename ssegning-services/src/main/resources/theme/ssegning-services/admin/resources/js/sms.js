module.factory('SmsConfigs', function ($resource) {
    return $resource(authUrl + '/realms/:realm/sms-config/:config', {}, {
        create: {
            method: 'POST',
            params: {
                realm: '@realm',
                config: '',
            }
        },
        remove: {
            method: 'DELETE',
            params: {
                realm: '@realm',
                config: '@config'
            }
        },
        get: {
            isArray: true,
            method: 'GET',
            params: {
                realm: '@realm',
                config: '',
            }
        },
        toggleDefault: {
            method: 'PUT',
            params: {
                realm: '@realm',
                config: '@config'
            }
        }
    });
});

module.factory('SmsSenderTypes', function ($resource) {
    return $resource(authUrl + '/realms/:realm/sms-config/sender-types', {}, {
        get: {
            isArray: true,
            method: 'GET',
            params: {
                realm: '@realm',
            }
        }
    });
});

module.factory('SmsSenderProperties', function ($resource) {
    return $resource(authUrl + '/realms/:realm/sms-config/sender-types/:id/properties', {
        realm: '@realm',
        id: '@id',
    });
});

///


// Don't allow URI reserved characters
module.directive('kcNoAdd', function (Notifications, $translate) {
    return function ($scope, element) {
        element.bind("keypress", function (event) {
            var keyPressed = String.fromCharCode(event.which || event.keyCode || 0);

            // ] and ' can not be used inside a character set on POSIX and GNU
            if (keyPressed === 'add') {
                event.preventDefault();
                $scope.$apply(function () {
                    Notifications.warn($translate.instant('key-not-allowed-here', {character: keyPressed}));
                });
            }
        });
    };
});

///

module.factory('SmsConfigsLoader', function (Loader, SmsConfigs, $route, $q) {
    return Loader.get(SmsConfigs, function () {
        return {
            realm: $route.current.params.realm
        }
    });
});

module.factory('SmsSenderTypeLoader', function (Loader, SmsSenderTypes, $route, $q) {
    return Loader.get(SmsSenderTypes, function () {
        return {
            realm: $route.current.params.realm
        }
    });
});

module.factory('SmsSenderPropertyLoader', function (Loader, SmsSenderProperties, $route, $q) {
    return Loader.get(SmsSenderProperties, function () {
        return {
            realm: $route.current.params.realm,
            id: $route.current.params.providerId,
        }
    });
});

///

module.controller('SmsConfigCtrl', function ($scope, $route, realm, configs, senderTypes, SmsConfigs, Notifications, Dialog, $location, ComponentUtils) {

    $scope.create = $route.current.params.config === 'add';
    let config = $scope.create ? {map: {}} : configs.filter(c => c.alias === $route.current.params.config)[0];

    $scope.realm = realm;
    $scope.configs = configs;
    $scope.senderTypes = senderTypes;
    $scope.config = angular.copy(config);
    $scope.changed = false;
    $scope.senderDisplayName = config?.senderProviderId !== null ? senderTypes.filter(s => s.senderProviderId === config?.senderProviderId)[0] : null;

    $scope.$watch(function () {
        return $location.path();
    }, function () {
        $scope.path = $location.path().substring(1).split("/");
    });

    $scope.$watch('config', function () {
        if (!angular.equals($scope.config, config)) {
            $scope.changed = true;
        }
    }, true);

    $scope.$watch('configs', function () {
        if (!angular.equals($scope.configs, configs)) {
            $scope.changed = true;
        }
    }, true);

    $scope.save = function () {
        if ($scope.senderDisplayName == null) {
            return;
        }
        let configCopy = angular.copy($scope.config);

        for (let i in configCopy.map) {
            let _value = configCopy.map[i]
            if (!Array.isArray(_value)) {
                configCopy.map[i] = [_value]
            }
        }

        SmsConfigs.create({
            realm: realm.realm,
        }, configCopy, function () {
            $scope.changed = false;
            config = angular.copy($scope.config);
            $location.url("/realms/" + realm.realm + '/sms-config');
            Notifications.success("Your changes have been saved.");
        });
    };

    $scope.makeDefault = function (config) {
        let copy = angular.copy(config);

        SmsConfigs.toggleDefault({realm: realm.realm, config: copy.alias}, copy, function () {
            Notifications.success("Auth requirement updated");
            $scope.configs = angular.copy($scope.configs.map(c => {
                if (c.alias === copy.alias) {
                    c.default = true;
                } else {
                    c.default = false;
                }
                return c;
            }));
        });

    };

    $scope.reset = function () {
        $scope.config = angular.copy(config);
        $scope.changed = false;
    };

    $scope.cancel = function () {
        //$location.url("/realms");
        window.history.back();
    };

    $scope.changeSenderChange = function () {
        let senderDisplayName = $scope.senderDisplayName
        if (senderDisplayName !== null) {
            $scope.config.senderProviderId = senderDisplayName.senderProviderId
        } else {
            $scope.config.senderProviderId = null
        }
    }

    $scope.remove = function () {
        Dialog.confirmDelete($scope.config.alias, 'config', function () {
            SmsConfigs.remove({realm: realm.realm, config: $scope.config.alias}, function () {
                Notifications.success("The config has been deleted.");
                $location.url("/realms/" + realm.realm + '/sms-config');
            });
        });
    };

});

///

module.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/realms/:realm/sms-config', {
            templateUrl: resourceUrl + '/partials/sms-configs.html',
            resolve: {
                realm: function (RealmLoader) {
                    return RealmLoader();
                },
                configs: function (SmsConfigsLoader) {
                    return SmsConfigsLoader();
                },
                senderTypes: function (SmsSenderTypeLoader) {
                    return SmsSenderTypeLoader();
                },
            },
            controller: 'SmsConfigCtrl',
        })
        .when('/realms/:realm/sms-config/:config', {
            templateUrl: resourceUrl + '/partials/sms-config.html',
            resolve: {
                realm: function (RealmLoader) {
                    return RealmLoader();
                },
                configs: function (SmsConfigsLoader) {
                    return SmsConfigsLoader();
                },
                senderTypes: function (SmsSenderTypeLoader) {
                    return SmsSenderTypeLoader();
                },
            },
            controller: 'SmsConfigCtrl',
        });
}]);
