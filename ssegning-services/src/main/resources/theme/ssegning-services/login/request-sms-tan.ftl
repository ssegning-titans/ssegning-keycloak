<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('smsTan'); section>
    <#if section="header">
        Sms Code
    <#elseif section="form">
        <style>
            /* Chrome, Safari, Edge, Opera */
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            /* Firefox */
            input[type=number] {
                -moz-appearance: textfield;
            }
        </style>

        <form id="kc-smsTan-login-form" class="${properties.kcFormClass!}" action="${url.loginAction}"
              method="post">
            <h2>A Code was send to the phone number <b>${phoneNumber}</b>. Please type it here to login</h2>

            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="smsTan" class="${properties.kcLabelClass!}">${msg("loginSmsTanOneTime")}</label>
                </div>

                <div class="${properties.kcInputWrapperClass!}">
                    <input id="smsTan" name="smsTan" autocomplete="off" class="${properties.kcInputClass!}" type="number"
                           autofocus aria-invalid="<#if messagesPerField.existsError('smsTan')>true</#if>"/>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <input
                            class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}"
                            name="login" id="kc-login" type="submit" value="${msg("doLogIn")}" />
                </div>
            </div>
        </form>

        <form class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                <input
                        class="${properties.kcButtonClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}"
                        name="resend" id="kc-login-resend-tan" type="submit" value="${msg("reSendMobileTan")}" />
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
