package com.ssegning.sschool.keycloak.account.model

import com.ssegning.sschool.keycloak.account.client.model.ApiAccount
import com.ssegning.sschool.keycloak.account.client.model.ApiUpdateAccount
import com.ssegning.sschool.keycloak.account.service.ApiService
import feign.FeignException
import org.apache.commons.lang.StringUtils
import org.keycloak.common.util.MultivaluedHashMap
import org.keycloak.component.ComponentModel
import org.keycloak.models.GroupModel
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.storage.StorageId
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage
import java.util.stream.Stream

class SchoolAccount(
    session: KeycloakSession,
    realm: RealmModel,
    model: ComponentModel,
    private var account: ApiAccount,
    private val apiService: ApiService,
) : AbstractUserAdapterFederatedStorage(session, realm, model) {

    override fun getId(): String {
        if (storageId == null) {
            storageId = StorageId(storageProviderModel.id, account.id)
        }
        return storageId.id
    }

    override fun getAttributes(): Map<String, List<String>> {
        val map: MultivaluedHashMap<String, String> = MultivaluedHashMap<String, String>()
        map[FIRST_NAME] = listOf(account.name)
        map[LAST_NAME] = listOf(account.lastName)
        map[EMAIL] = listOf(account.email)
        map[USERNAME] = listOf(account.email)
        return map
    }

    override fun getUsername(): String? = attributes[USERNAME]!!.firstOrNull()

    override fun setUsername(username: String) = setAttribute(USERNAME, mutableListOf(username))

    override fun getFirstName(): String? = getFirstAttribute(FIRST_NAME)

    override fun setFirstName(firstName: String) = setSingleAttribute(FIRST_NAME, firstName)

    override fun getLastName(): String? = getFirstAttribute(LAST_NAME)

    override fun setLastName(lastName: String) = setSingleAttribute(LAST_NAME, lastName)

    override fun getEmail(): String? = getFirstAttribute(EMAIL)

    override fun setEmail(email: String) = setSingleAttribute(EMAIL, email.lowercase())

    override fun getFirstAttribute(name: String): String? {
        val attributes = attributes
        val values = attributes[name]
        return if (values?.isNotEmpty() == true) values[0] else null
    }

    override fun isEnabled() = true

    override fun setEnabled(enabled: Boolean) {
    }

    override fun isEmailVerified(): Boolean = account.emailVerified == true

    override fun setEmailVerified(verified: Boolean) {
        setSingleAttribute(EMAIL_VERIFIED, verified.toString())
    }

    override fun getGroupsStream(): Stream<GroupModel> = Stream.of()

    override fun getGroupsStream(search: String?, first: Int, max: Int): Stream<GroupModel> = Stream.of()

    override fun getGroupsCount(): Long = 0

    override fun getGroupsCountByNameContaining(search: String?): Long = 0

    override fun getCreatedTimestamp(): Long = account.createdAt.time

    override fun setAttribute(name: String, values: List<String>?) {
        if (!StringUtils.isEmpty(name)
            && values?.isNotEmpty() == true && !StringUtils.isEmpty(values[0])
        ) {
            val value = values[0]
            setSingleAttribute(name, value)
        }
    }

    override fun setSingleAttribute(name: String, value: String) {
        if (!StringUtils.isEmpty(value)) {
            buildChangeData(name, value)
        }
    }

    private fun buildChangeData(name: String, value: String): Boolean {
        val request = ApiUpdateAccount()
        request.id = account.id!!
        when (name) {
            FIRST_NAME -> request.name = value
            LAST_NAME -> request.lastName = value
            EMAIL, USERNAME -> request.email = value
            EMAIL_VERIFIED -> request.emailVerified = value == "true"
            else -> return false
        }

        return try {
            account = apiService.updateAccount(request)
            true
        } catch (e: FeignException.FeignClientException) {
            false
        }
    }

    override fun toString(): String = "DjallerAccount(id=$id, attributes=$attributes)"

}
