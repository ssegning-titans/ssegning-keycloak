package com.ssegning.sschool.keycloak.account.storage

import com.ssegning.sschool.keycloak.account.client.model.ApiAccount
import com.ssegning.sschool.keycloak.account.model.SchoolAccount
import com.ssegning.sschool.keycloak.account.service.ApiService
import feign.FeignException
import org.keycloak.component.ComponentModel
import org.keycloak.models.GroupModel
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.models.UserModel
import org.keycloak.storage.StorageId
import org.keycloak.storage.UserStorageProvider
import org.keycloak.storage.user.UserLookupProvider
import org.keycloak.storage.user.UserQueryProvider
import org.keycloak.storage.user.UserRegistrationProvider
import java.util.stream.Stream

class AccountStorageProvider(
    private val apiService: ApiService,
    private val session: KeycloakSession,
    private val model: ComponentModel,
) : UserStorageProvider,
    UserLookupProvider,
    UserQueryProvider,
    UserRegistrationProvider {

    override fun close() {}

    /// Account

    override fun getUserById(realm: RealmModel, id: String): UserModel? {
        val account = apiService.getAccountById(StorageId.externalId(id))
        return toModel(session, realm, model, account)
    }

    override fun getUserByUsername(realm: RealmModel, username: String): UserModel? {
        val account = apiService.getAccount(username)
        return toModel(session, realm, model, account)
    }

    override fun getUserByEmail(realm: RealmModel, email: String): UserModel? {
        val account = apiService.getAccount(email)
        return toModel(session, realm, model, account)
    }

    /// Queries

    override fun getUsersCount(realm: RealmModel): Int = 0

    override fun getUsersStream(realm: RealmModel): Stream<UserModel> = getUsersStream(realm, 0, 10)

    override fun getUsersStream(realm: RealmModel, firstResult: Int, maxResults: Int): Stream<UserModel> =
        searchForUserStream(realm, mutableMapOf(), firstResult, maxResults)

    override fun searchForUserStream(realm: RealmModel, search: String): Stream<UserModel> =
        searchForUserStream(realm, search, 0, 10)

    override fun searchForUserStream(
        realm: RealmModel,
        search: String,
        firstResult: Int,
        maxResults: Int
    ): Stream<UserModel> =
        searchForUserStream(realm, mutableMapOf("query" to search), firstResult, maxResults)

    override fun searchForUserStream(realm: RealmModel, params: MutableMap<String, String>): Stream<UserModel> =
        searchForUserStream(realm, params, 0, 10)

    override fun searchForUserStream(
        realm: RealmModel,
        params: MutableMap<String, String>,
        firstResult: Int,
        maxResults: Int
    ): Stream<UserModel> = apiService
        .getMultipleAccount(params, firstResult, maxResults)
        .stream()
        .map { toModel(session, realm, model, it)!! }

    override fun getGroupMembersStream(realm: RealmModel, group: GroupModel): Stream<UserModel> {
        return Stream.empty()
    }

    override fun getGroupMembersStream(
        realm: RealmModel,
        group: GroupModel,
        firstResult: Int,
        maxResults: Int
    ): Stream<UserModel> {
        return Stream.empty()
    }

    override fun searchForUserByUserAttributeStream(
        realm: RealmModel,
        attrName: String,
        attrValue: String,
    ): Stream<UserModel> {
        return Stream.empty()
    }

    ///

    private fun toModel(
        session: KeycloakSession,
        realm: RealmModel,
        model: ComponentModel,
        account: ApiAccount?,
    ): UserModel? = if (account != null) SchoolAccount(session, realm, model, account, apiService) else null

    override fun addUser(realm: RealmModel, username: String): UserModel? = try {
        val created = apiService.createAccount(username)
        toModel(session, realm, model, created)
    } catch (e: FeignException.FeignClientException) {
        null
    }

    override fun removeUser(realm: RealmModel, user: UserModel): Boolean = try {
        apiService.deleteAccount(StorageId.externalId(user.id))
        true
    } catch (e: FeignException.NotFound) {
        false
    }

}
