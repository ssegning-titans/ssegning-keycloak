package com.ssegning.sschool.keycloak.account.service

import com.ssegning.sschool.keycloak.account.client.api.AccountsApi
import com.ssegning.sschool.keycloak.account.client.handler.ApiClient
import com.ssegning.sschool.keycloak.account.client.model.ApiAccount
import com.ssegning.sschool.keycloak.account.client.model.ApiCreateAccountInput
import com.ssegning.sschool.keycloak.account.client.model.ApiUpdateAccount
import feign.FeignException

class ApiService(val client: ApiClient) {

    private var accountApi = client.feignBuilder.target(AccountsApi::class.java, client.basePath)

    fun getAccountById(externalId: String?): ApiAccount? = try {
        accountApi.findOneAccount(externalId)
    } catch (e: FeignException) {
        if (e.status() == 404) null else throw e
    }

    fun createAccount(email: String): ApiAccount {
        val body = ApiCreateAccountInput()
        body.email = email
        body.emailVerified = false
        return accountApi.createAccount(body)
    }

    fun getAccount(email: String): ApiAccount? = try {
        accountApi.findByEmail(email)
    } catch (e: FeignException) {
        if (e.status() == 404) null else throw e
    }

    fun getMultipleAccount(
        params: MutableMap<String, String>,
        firstResult: Int,
        maxResults: Int
    ): Collection<ApiAccount> = accountApi.findAllAccounts(
        firstResult.toBigDecimal(),
        maxResults.toBigDecimal(),
        params["query"],
    )

    fun deleteAccount(externalId: String) = accountApi.deleteOneAccount(externalId)

    fun updateAccount(request: ApiUpdateAccount): ApiAccount =
        accountApi.updateAccount(request)

}
