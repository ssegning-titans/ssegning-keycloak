# SSegning Auth (Keycloak)

```bash
helm dependency update ssegning-keycloak
````

```bash
helm dependency build ssegning-keycloak
````

```bash
helm install ssegning-keycloak ssegning-keycloak -n auth
````

```bash
helm upgrade ssegning-keycloak ./ssegning-keycloak -n auth
````

```bash
helm delete ssegning-keycloak -n auth
````

## Links
- https://maven.apache.org/plugins/maven-assembly-plugin/assembly.html