package com.ssegning.djaller.keycloak.account.config

import com.ssegning.djaller.keycloak.account.client.model.ApiAccountDto

const val PROPERTY_PHONE_NUMBER = "phoneNumber"
const val PROPERTY_PHONE_NUMBER_VALIDATED = "phoneNumberValidated"
const val PROPERTY_GENDER = ApiAccountDto.JSON_PROPERTY_GENDER
const val PROPERTY_LOCALE = ApiAccountDto.JSON_PROPERTY_LOCALE
const val PROPERTY_ENABLED = "enabled"
