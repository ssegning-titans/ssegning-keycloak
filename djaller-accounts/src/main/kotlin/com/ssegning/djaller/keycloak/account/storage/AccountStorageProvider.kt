package com.ssegning.djaller.keycloak.account.storage

import com.ssegning.djaller.keycloak.account.client.model.ApiAccountDto
import com.ssegning.djaller.keycloak.account.model.DjallerAccount
import com.ssegning.djaller.keycloak.account.service.ApiService
import feign.FeignException
import org.keycloak.component.ComponentModel
import org.keycloak.credential.CredentialInput
import org.keycloak.credential.CredentialInputUpdater
import org.keycloak.credential.CredentialInputValidator
import org.keycloak.models.GroupModel
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.models.UserModel
import org.keycloak.models.credential.PasswordCredentialModel
import org.keycloak.storage.StorageId
import org.keycloak.storage.UserStorageProvider
import org.keycloak.storage.user.UserLookupProvider
import org.keycloak.storage.user.UserQueryProvider
import org.keycloak.storage.user.UserRegistrationProvider
import java.util.stream.Stream

class AccountStorageProvider(
    private val apiService: ApiService,
    private val session: KeycloakSession,
    private val model: ComponentModel,
) : UserStorageProvider,
    UserLookupProvider,
    UserQueryProvider,
    CredentialInputValidator,
    CredentialInputUpdater,
    UserRegistrationProvider {

    override fun close() {}

    /// Account

    override fun getUserById(realm: RealmModel, id: String): UserModel? {
        val account = apiService.getAccountById(StorageId.externalId(id))
        return toModel(session, realm, model, account)
    }

    override fun getUserByUsername(realm: RealmModel, username: String): UserModel? {
        val account = apiService.getAccountByPhoneNumber(username)
        return toModel(session, realm, model, account)
    }

    override fun getUserByEmail(realm: RealmModel, email: String): UserModel? {
        val account = apiService.getAccount(email)
        return toModel(session, realm, model, account)
    }

    /// Credentials

    override fun supportsCredentialType(credentialType: String): Boolean =
        credentialType == PasswordCredentialModel.TYPE

    override fun updateCredential(realm: RealmModel, user: UserModel, input: CredentialInput): Boolean = try {
        apiService.createPassword(
            StorageId.externalId(user.id),
            input.challengeResponse
        )
        true
    } catch (e: FeignException.FeignClientException) {
        false
    }

    override fun disableCredentialType(realm: RealmModel, user: UserModel, credentialType: String): Unit =
        apiService.disablePassword(StorageId.externalId(user.id))

    override fun getDisableableCredentialTypesStream(realm: RealmModel, user: UserModel): Stream<String> =
        Stream.empty()

    override fun isConfiguredFor(realm: RealmModel, user: UserModel, credentialType: String): Boolean =
        supportsCredentialType(credentialType)

    override fun isValid(realm: RealmModel, user: UserModel, credentialInput: CredentialInput): Boolean =
        apiService.validateCred(StorageId.externalId(user.id), credentialInput.challengeResponse)

    /// Queries

    override fun getUsersCount(realm: RealmModel): Int = 0

    override fun getUsersStream(realm: RealmModel): Stream<UserModel> = getUsersStream(realm, 0, 10)

    override fun getUsersStream(realm: RealmModel, firstResult: Int, maxResults: Int) =
        searchForUserStream(realm, mutableMapOf(), firstResult, maxResults)

    override fun searchForUserStream(realm: RealmModel, search: String) =
        searchForUserStream(realm, search, 0, 10)

    override fun searchForUserStream(
        realm: RealmModel,
        search: String,
        firstResult: Int,
        maxResults: Int
    ) =
        searchForUserStream(
            realm,
            mutableMapOf("query" to search),
            firstResult, maxResults
        )

    override fun searchForUserStream(realm: RealmModel, params: MutableMap<String, String>) =
        searchForUserStream(realm, params, 0, 10)

    override fun searchForUserStream(
        realm: RealmModel,
        params: MutableMap<String, String>,
        firstResult: Int,
        maxResults: Int
    ): Stream<UserModel> = apiService
        .getMultipleAccount(params, firstResult, maxResults)
        .stream()
        .map { toModel(session, realm, model, it)!! }

    override fun getGroupMembersStream(realm: RealmModel, group: GroupModel): Stream<UserModel> =
        Stream.empty()

    override fun getGroupMembersStream(
        realm: RealmModel,
        group: GroupModel,
        firstResult: Int,
        maxResults: Int
    ): Stream<UserModel> = Stream.empty()

    override fun searchForUserByUserAttributeStream(
        realm: RealmModel,
        attrName: String,
        attrValue: String,
    ): Stream<UserModel> = Stream.empty()

    ///

    private fun toModel(
        session: KeycloakSession,
        realm: RealmModel,
        model: ComponentModel,
        account: ApiAccountDto?,
    ): UserModel? = if (account != null) DjallerAccount(session, realm, model, account, apiService) else null

    override fun addUser(realm: RealmModel, username: String): UserModel? = try {
        val created = apiService.createAccount(username)
        toModel(session, realm, model, created)
    } catch (e: FeignException.FeignClientException) {
        null
    }

    override fun removeUser(realm: RealmModel, user: UserModel): Boolean = try {
        apiService.deleteAccount(StorageId.externalId(user.id))
        true
    } catch (e: FeignException.NotFound) {
        false
    }

}
