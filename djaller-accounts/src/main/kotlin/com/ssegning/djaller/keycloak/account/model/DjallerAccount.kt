package com.ssegning.djaller.keycloak.account.model

import com.ssegning.djaller.keycloak.account.client.model.ApiAccountDto
import com.ssegning.djaller.keycloak.account.config.*
import com.ssegning.djaller.keycloak.account.service.ApiService
import feign.FeignException
import org.apache.commons.lang.StringUtils
import org.keycloak.common.util.MultivaluedHashMap
import org.keycloak.component.ComponentModel
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.storage.StorageId
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage

class DjallerAccount(
    session: KeycloakSession,
    realm: RealmModel,
    model: ComponentModel,
    private var account: ApiAccountDto,
    private val apiService: ApiService,
) : AbstractUserAdapterFederatedStorage(session, realm, model) {

    override fun getId(): String {
        if (storageId == null) {
            storageId = StorageId(storageProviderModel.id, account.id!!.toString())
        }
        return storageId.id
    }

    override fun getAttributes(): Map<String, List<String>> {
        val map: MultivaluedHashMap<String, String> = MultivaluedHashMap<String, String>()
        map[PROPERTY_PHONE_NUMBER] = listOf(account.phoneNumber)
        map[PROPERTY_PHONE_NUMBER_VALIDATED] = listOf(account.phoneNumberValidated?.toString())
        map[PROPERTY_GENDER] = listOf(account.gender?.name)
        map[PROPERTY_LOCALE] = listOf(account.locale)

        map[FIRST_NAME] = listOf(account.firstName)
        map[LAST_NAME] = listOf(account.lastName)
        map[EMAIL] = listOf(account.email)
        map[USERNAME] = listOf(account.email)
        return map
    }

    override fun getUsername(): String? = attributes[USERNAME]!!.firstOrNull()

    override fun setUsername(username: String) = setAttribute(USERNAME, mutableListOf(username))

    override fun getFirstName(): String? = getFirstAttribute(FIRST_NAME)

    override fun setFirstName(firstName: String) = setSingleAttribute(FIRST_NAME, firstName)

    override fun getLastName(): String? = getFirstAttribute(LAST_NAME)

    override fun setLastName(lastName: String) = setSingleAttribute(LAST_NAME, lastName)

    override fun getEmail(): String? = getFirstAttribute(EMAIL)

    override fun setEmail(email: String) = setSingleAttribute(EMAIL, email.lowercase())

    override fun getFirstAttribute(name: String): String? {
        val attributes = attributes
        val values = attributes[name]
        return if (values?.isNotEmpty() == true) values[0] else null
    }

    override fun isEnabled() = account.status == ApiAccountDto.StatusEnum.ACTIVE

    override fun setEnabled(enabled: Boolean) = setSingleAttribute(PROPERTY_ENABLED, enabled.toString())

    override fun isEmailVerified(): Boolean = account.emailValidated == true

    override fun setEmailVerified(verified: Boolean) = setSingleAttribute(EMAIL_VERIFIED, verified.toString())

    override fun getGroupsCount(): Long = 0

    override fun getGroupsCountByNameContaining(search: String?): Long = 0

    override fun getCreatedTimestamp(): Long = account.createdDate?.time!!

    override fun setAttribute(name: String, values: List<String>?) {
        if (!StringUtils.isEmpty(name)
            && values?.isNotEmpty() == true && !StringUtils.isEmpty(values[0])
        ) {
            val value = values[0]
            setSingleAttribute(name, value)
        }
    }

    override fun setSingleAttribute(name: String, value: String) {
        if (!StringUtils.isEmpty(value)) {
            buildChangeData(name, value)
        }
    }

    private fun buildChangeData(name: String, value: String): Boolean {
        val request = ApiAccountDto()
        when (name) {
            PROPERTY_PHONE_NUMBER_VALIDATED -> request.phoneNumberValidated = value == "true"
            PROPERTY_PHONE_NUMBER -> {
                request.phoneNumber = value
                request.phoneNumberValidated = false
            }

            PROPERTY_GENDER -> request.gender = ApiAccountDto.GenderEnum.fromValue(value)
            PROPERTY_LOCALE -> request.locale = value
            FIRST_NAME -> request.firstName = value
            LAST_NAME -> request.lastName = value
            EMAIL, USERNAME -> request.email = value
            EMAIL_VERIFIED -> request.emailValidated = value == "true"
            PROPERTY_ENABLED -> request.status =
                if (value == "true") ApiAccountDto.StatusEnum.ACTIVE else ApiAccountDto.StatusEnum.LOCKED

            else -> return false
        }

        return try {
            account = apiService.updateAccount(account.id!!.toString(), request)
            true
        } catch (e: FeignException.FeignClientException) {
            false
        }
    }

    override fun toString(): String = "DjallerAccount(id=$id, attributes=$attributes)"

}
