package com.ssegning.djaller.keycloak.account.service

import com.ssegning.djaller.keycloak.account.client.api.AccountControllerApi
import com.ssegning.djaller.keycloak.account.client.api.PasswordControllerApi
import com.ssegning.djaller.keycloak.account.client.handler.ApiClient
import com.ssegning.djaller.keycloak.account.client.model.ApiAccountDto
import com.ssegning.djaller.keycloak.account.client.model.ApiPasswordDto
import feign.FeignException

class ApiService(val client: ApiClient) {

    private val accountApi = client.feignBuilder.target(AccountControllerApi::class.java, client.basePath)
    private val passwordApi = client.feignBuilder.target(PasswordControllerApi::class.java, client.basePath)

    fun getAccountById(externalId: String?): ApiAccountDto? = try {
        accountApi.get1(externalId)
    } catch (e: FeignException) {
        if (e.status() == 404) null else throw e
    }

    fun createAccount(email: String): ApiAccountDto {
        val body = ApiAccountDto()
        body.email = email
        body.emailValidated = false
        body.status = ApiAccountDto.StatusEnum.ACTIVE
        body.phoneNumberValidated = false
        return accountApi.add1(body)
    }

    fun getAccountByPhoneNumber(username: String): ApiAccountDto? = try {
        accountApi.findByPhoneNumber(username)
    } catch (e: FeignException) {
        if (e.status() == 404) null else throw e
    }

    fun getAccount(email: String): ApiAccountDto? = try {
        accountApi.findByEmail(email)
    } catch (e: FeignException) {
        if (e.status() == 404) null else throw e
    }

    fun createPassword(externalId: String, challengeResponse: String): ApiPasswordDto {
        val body = ApiPasswordDto()
        body.accountId = externalId
        body.hash = challengeResponse

        return passwordApi.add(body)
    }

    fun disablePassword(externalId: String): Unit = try {
        accountApi.remove1(externalId)
    } catch (e: FeignException) {
        if (e.status() == 404) Unit else throw e
    }

    fun validateCred(externalId: String, challengeResponse: String): Boolean =
        passwordApi.verify(challengeResponse, externalId)

    fun getMultipleAccount(
        params: MutableMap<String, String>,
        firstResult: Int,
        maxResults: Int
    ): Collection<ApiAccountDto> = if (params["query"] != null) accountApi.findAllLikeQueries(
        params["query"],
        firstResult,
        maxResults,
        mutableListOf("createdDate,desc")
    ) else accountApi.findAll1(firstResult, maxResults, mutableListOf("createdDate,desc"))

    fun deleteAccount(externalId: String) = accountApi.remove1(externalId)

    fun updateAccount(id: String, request: ApiAccountDto): ApiAccountDto = accountApi.patch1(id, request)

}
