FROM adoptopenjdk/maven-openjdk11

ENV NODE_VERSION=14

RUN apt-get update -y && \
     apt-get upgrade -y && \
     apt-get dist-upgrade -y && \
     apt-get -y autoremove && \
     apt-get clean

RUN apt-get install -y p7zip \
    curl \
    wget \
    p7zip-full \
    unace \
    zip \
    unzip \
    xz-utils \
    sharutils \
    uudeview \
    mpack \
    arj \
    cabextract \
    file-roller

# install nvm / node
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash
RUN apt-get install nodejs -y
RUN npm i -g yarn

RUN mvn --version && node --version && npm --version && wget --version && curl --version

CMD ["bash"]
