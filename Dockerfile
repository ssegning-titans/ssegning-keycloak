FROM quay.io/keycloak/keycloak:20.0.2

ENV KEYCLOAK_DIR /opt/keycloak
ENV KC_DB postgres
ENV KC_PROXY edge

LABEL maintainer="Stephane, Segning Lambou <me@ssegning.com>"

USER 0

RUN mkdir -p ${KEYCLOAK_DIR}/data && chmod 777 ${KEYCLOAK_DIR}/data

COPY ./react-projects/djaller-theme/build_keycloak/target/djaller-theme-keycloak-theme-0.2.0.jar $KEYCLOAK_DIR/providers/djaller-theme.jar

COPY ./common-spi/target/common-spi-jar-with-dependencies-and-exclude-classes.jar ${KEYCLOAK_DIR}/providers/common-spi.jar
COPY ./djaller-accounts/target/djaller-accounts.jar ${KEYCLOAK_DIR}/providers/djaller-accounts.jar
COPY ./sschool-accounts/target/sschool-accounts.jar ${KEYCLOAK_DIR}/providers/sschool-accounts.jar
COPY ./sshop-accounts/target/sshop-accounts.jar ${KEYCLOAK_DIR}/providers/sshop-accounts.jar
COPY ./ssegning-services/target/ssegning-services.jar ${KEYCLOAK_DIR}/providers/ssegning-services.jar

RUN ${KEYCLOAK_DIR}/bin/kc.sh build

USER 1000
