import {getKcContext, KcContextBase} from "keycloakify";

export declare type PhoneTanType = {
    pageId: "request-sms-tan.ftl";
    phoneNumber: string;
}

export declare type PhoneNumberType = {
    pageId: "request-phone-number.ftl";
}

export declare type LoginPassword = {
    pageId: "login-password.ftl";
} & KcContextBase.Common & {
    url: {
        loginResetCredentialsUrl: string;
    };
    realm: {
        resetPasswordAllowed: true,
    }
}

export declare type LoginUpdatePassword = {
    pageId: "login-update-password.ftl";
} & KcContextBase.Common & {
    isAppInitiatedAction: boolean;
    username: string;
}

export declare type LoginUsername = {
    pageId: "login-username.ftl";
} & Omit<KcContextBase.Login, 'pageId'>

export declare type LoginOauthGrant = {
    pageId: "login-oauth-grant.ftl";
    client: {
        baseUrl?: string,
        name?: string,
        clientId?: string,
    },
    oauth: {
        clientScopesRequested: {
            consentScreenText: string,
            description: string,
        }[],
        code: string,
    },
    url: {
        oauthAction: string
    }
}

declare type LocalContextType =
    LoginUpdatePassword
    | LoginPassword
    | PhoneNumberType
    | PhoneTanType
    | LoginUsername
    | LoginOauthGrant;

export const {kcContext} = getKcContext<LocalContextType>({
    mockData: [
        {
            "pageId": "login.ftl",
            "locale": {
                //When we test the login page we do it in french
                "currentLanguageTag": "fr",
            },
        },
        {
            //NOTE: You will either use register.ftl (legacy) or register-user-profile.ftl, not both
            "pageId": "register-user-profile.ftl",
            "locale": {
                "currentLanguageTag": "fr"
            },
            "profile": {
                "attributes": [
                    {
                        "validators": {
                            "pattern": {
                                "pattern": "^[a-zA-Z0-9]+$",
                                "ignore.empty.value": true,
                                // eslint-disable-next-line no-template-curly-in-string
                                "error-message": "${alphanumericalCharsOnly}",
                            },
                        },
                        //NOTE: To override the default mock value
                        "value": undefined,
                        "name": "username"
                    },
                    {
                        "validators": {
                            "options": {
                                "options": ["male", "female", "non-binary", "transgender", "intersex", "non_communicated"]
                            }
                        },
                        // eslint-disable-next-line no-template-curly-in-string
                        "displayName": "${gender}",
                        "annotations": {},
                        "required": true,
                        "groupAnnotations": {},
                        "readOnly": false,
                        "name": "gender"
                    }
                ]
            }
        },
        {
            pageId: 'login-oauth-grant.ftl',
            client: {},
            oauth: {
                clientScopesRequested: [
                    {consentScreenText: 'First', description: 'Some description'}
                ]
            },
            url: {}
        },
        {
            pageId: 'login-username.ftl',
            client: {},
            url: {},
            social: {},
            realm: {
                password: true,
                rememberMe: true,
                registrationAllowed: true,
                resetPasswordAllowed: true,
            },
            usernameEditDisabled: false,
            login: {},
            auth: {},
            registrationDisabled: false,
        },
        {
            pageId: 'request-phone-number.ftl',
        },
        {
            pageId: "login-password.ftl",
            realm: {
                resetPasswordAllowed: true,
            },
        },
        {
            pageId: "login-update-password.ftl",
            isAppInitiatedAction: true,
        }
    ],
    mockPageId: 'login.ftl'
});

export type KcContext = NonNullable<typeof kcContext>;
