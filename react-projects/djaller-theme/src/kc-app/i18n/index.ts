import en from './en.json';
import fr from './fr.json';
import es from './es.json';
import de from './de.json';

export const i18nKeys: any = {en, fr, de, es};
