import {lazy, memo, Suspense} from "react";
import type {KcContext} from "./kc-context";
import KcAppBase, {defaultKcProps, useI18n} from "keycloakify";

const ErrorPage = lazy(() => import("../pages/error"));
const InfoApp = lazy(() => import("../pages/info"));
const LoginIdpLinkConfirmPage = lazy(() => import("../pages/login-idp-link-confirm-page"));
const LoginOauthGrantPage = lazy(() => import("../pages/login-oauth-grant"));
const LoginOtpPage = lazy(() => import("../pages/login-otp"));
const LoginPage = lazy(() => import("../pages/login"));
const LoginPageExpiredPage = lazy(() => import("../pages/login-page-expired"));
const LoginPasswordPage = lazy(() => import("../pages/login-password"));
const LoginResetPasswordPage = lazy(() => import("../pages/login-reset-password"));
const LoginUpdateProfilePage = lazy(() => import("../pages/login-update-profile"));
const LoginVerifyEmailPage = lazy(() => import("../pages/login-verify-email"));
const RegisterPage = lazy(() => import("../pages/register"));
const RequestPhoneNumber = lazy(() => import("../pages/request-phone-number"));
const RequestSmsTan = lazy(() => import("../pages/request-sms-tan"));
const LoginUsernamePage = lazy(() => import("../pages/login-username"));
const LoginUpdatePasswordPage = lazy(() => import("../pages/login-update-password"));
const TermsPage = lazy(() => import("../pages/terms"));

const KcApp = memo(({kcContext}: { kcContext: KcContext; }) => {
    const i18n = useI18n({kcContext, extraMessages: {}});

    //NOTE: Locales not yet downloaded
    if (i18n === null) {
        return null;
    }

    const props = {
        i18n,
        ...defaultKcProps,
        // NOTE: The classes are defined in ./KcApp.css
        // "kcHeaderWrapperClass": "my-color my-font",
    };

    return (
        <Suspense>
            {(() => {
                switch (kcContext.pageId) {
                    case "login.ftl":
                        return <LoginPage {...{kcContext, ...props}} />;
                    case "register.ftl":
                        return <RegisterPage {...{kcContext, ...props}} />;
                    case "info.ftl":
                        return <InfoApp {...{kcContext, ...props}} />;
                    case "error.ftl":
                        return <ErrorPage {...{kcContext, ...props}} />;
                    case "login-reset-password.ftl":
                        return <LoginResetPasswordPage {...{kcContext, ...props}} />;
                    case "login-verify-email.ftl":
                        return <LoginVerifyEmailPage {...{kcContext, ...props}} />;
                    case "terms.ftl":
                        return <TermsPage {...{kcContext, ...props}} />;
                    case "login-otp.ftl":
                        return <LoginOtpPage {...{kcContext, ...props}} />;
                    case "login-update-profile.ftl":
                        return <LoginUpdateProfilePage {...{kcContext, ...props}} />;
                    case "login-idp-link-confirm.ftl":
                        return <LoginIdpLinkConfirmPage {...{kcContext, ...props}} />;
                    case "request-sms-tan.ftl":
                        return <RequestSmsTan {...{kcContext, ...props}} />;
                    case "request-phone-number.ftl":
                        return <RequestPhoneNumber {...{kcContext, ...props}} />;
                    case "login-update-password.ftl":
                        return <LoginUpdatePasswordPage {...{kcContext, ...props}} />;
                    case "login-password.ftl":
                        return <LoginPasswordPage {...{kcContext, ...props}} />;
                    case "login-page-expired.ftl":
                        return <LoginPageExpiredPage {...{kcContext, ...props}} />;
                    case "login-oauth-grant.ftl":
                        return <LoginOauthGrantPage {...{kcContext, ...props}} />;
                    case "login-username.ftl":
                        return <LoginUsernamePage {...{kcContext, ...props}} />;
                    default:
                        return <KcAppBase {...{kcContext, ...props}} />;
                }
            })()}
        </Suspense>
    )
});

export default KcApp;
