import React, {lazy, StrictMode, Suspense} from 'react';
import {kcContext} from './kc-app/kc-context';
import {createRoot} from 'react-dom/client';
import {AppTheme} from './components/theme';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import './index.css';

const App = lazy(() => import('./App'));
const KcApp = lazy(() => import('./kc-app/kc-app'));

if (kcContext !== undefined) {
    console.log(kcContext);
}

createRoot(document.getElementById("root")!).render(
    <StrictMode>
        <AppTheme>
            <Suspense>
                {kcContext === undefined
                    ? <App/>
                    : <KcApp kcContext={kcContext}/>}
            </Suspense>
        </AppTheme>
    </StrictMode>
);
