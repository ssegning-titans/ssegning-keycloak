import {unstable_createMuiStrictModeTheme} from "@mui/material";

export const darkThemeOptions = unstable_createMuiStrictModeTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#9102ff',
        },
        background: {
            default: '#08000e',
            paper: '#18002c',
        },
        text: {
            primary: '#ffffff',
            secondary: '#d0d0d0',
            disabled: '#a0a0a0'
        },
        secondary: {
            main: '#E39719'
        },
        error: {
            main: '#F0625B'
        },
        warning: {
            main: '#E39719'
        },
        success: {
            main: '#19E36A'
        }
    },
});
