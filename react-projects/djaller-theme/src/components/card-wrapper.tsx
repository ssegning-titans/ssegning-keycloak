import {Box} from "@mui/material";

export default function CardWrapper({children}) {
    return (
        <Box id="kc-content">
            <Box id="kc-content-wrapper">
                {children}
            </Box>
        </Box>
    );
}