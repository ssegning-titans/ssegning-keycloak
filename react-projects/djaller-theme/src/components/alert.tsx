import React, {useCallback, useState} from 'react';
import {Alert as MAlert, AlertTitle, Box, Collapse, IconButton} from "@mui/material";
import {Close} from "@mui/icons-material";

export function Alert({message}: any) {
    const [open, setOpen] = useState(true);
    const shouldClose = useCallback(() => setOpen(false), [setOpen]);
    return (
        <Collapse in={open}>
            <Box marginY='1rem'>
                <MAlert
                    action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={shouldClose}
                        >
                            <Close fontSize="inherit"/>
                        </IconButton>
                    }
                    severity={message.type}
                >
                    <AlertTitle>{(message.type as string).toLocaleUpperCase()}</AlertTitle>
                    <span dangerouslySetInnerHTML={{"__html": message.summary}}/>
                </MAlert>
            </Box>
        </Collapse>
    );
}
