import {darkThemeOptions} from "../hooks/theme";
import {CssBaseline, ThemeProvider} from "@mui/material";
import React from "react";

export function AppTheme({children}) {
    return (
        <ThemeProvider theme={darkThemeOptions}>
            <CssBaseline enableColorScheme/>
            {children}
        </ThemeProvider>
    );
}
