import type {ReactNode} from "react";
import {memo, MouseEvent, useCallback, useEffect, useState} from "react";
import {I18n, KcTemplateProps} from "keycloakify";
import {KcContext} from "../kc-app/kc-context";
import {Box, Button, Container, Grid, Link, Menu, MenuItem, Typography} from "@mui/material";
import {Alert} from "./alert";
import {Language} from "@mui/icons-material";
import whale from './whale.png';
import CardWrapper from "./card-wrapper";

const ITEM_HEIGHT = 72;

export type TemplateProps = {
    displayInfo?: boolean;
    displayMessage?: boolean;
    displayRequiredFields?: boolean;
    displayWide?: boolean;
    showAnotherWayIfPresent?: boolean;
    headerNode: ReactNode;
    showUsernameNode?: ReactNode;
    formNode: ReactNode;
    infoNode?: ReactNode;
} & { kcContext: KcContext; i18n: I18n; } & KcTemplateProps;

export const Template = memo((props: TemplateProps) => {
    const {
        displayInfo = false,
        displayMessage = true,
        displayRequiredFields = false,
        showAnotherWayIfPresent = true,
        headerNode,
        showUsernameNode = null,
        formNode,
        infoNode = null,
        kcContext,
    } = props;

    const {msg, changeLocale, labelBySupportedLanguageTag, currentLanguageTag} = props.i18n;

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const onLanguageChange = useCallback(
        (kcLanguageTag: string) => () => {
            setAnchorEl(null);
            if (kcLanguageTag != null) {
                changeLocale(kcLanguageTag);
            }
        }, [changeLocale]);

    const onTryAnotherWayClick = useCallback(() => {
        document.forms["kc-select-try-another-way-form" as never].submit();
        return false;
    }, []);

    const {
        realm, locale, auth,
        url, message, isAppInitiatedAction
    } = kcContext;

    useEffect(() => {
        if (!realm.internationalizationEnabled) {
            return;
        }
        // window.location.href = locale.supported.find(({languageTag}) => languageTag === locale.currentLanguageTag)!.url;

    }, [realm, locale]);

    const openMenu = useCallback((event: MouseEvent<HTMLButtonElement>) => setAnchorEl(event.currentTarget), []);

    useEffect(() => {
        console.log(
            "%cSSegning",
            "color:blue;font-family:system-ui;font-size:4rem;-webkit-text-stroke: 1px black;font-weight:bold"
        );
        console.log('Looking for Developers; Please write me at <p>selastlambou@gmail.com</p>');
    }, []);

    return (
        <Container component="main" maxWidth="xs">

            <Box
                display='flex'
                flexDirection='column'
                alignItems='center'
                alignContent='center'
                minHeight='100vh'
                justifyContent='center'
            >

                <Grid direction="row"
                      justifyContent="space-between"
                      alignItems="center"
                      container spacing={3}>
                    <Grid id="kc-header" component='div' xs item>
                        <Typography id="kc-header-wrapper" component="h5" variant="body2">
                            {msg("loginTitleHtml", realm.displayNameHtml ?? realm.displayName)}
                        </Typography>
                    </Grid>
                    {
                        (
                            realm.internationalizationEnabled &&
                            locale !== undefined &&
                            locale.supported.length > 1
                        ) &&
                        <Grid item>
                            <Button
                                size="small"
                                color='inherit'
                                variant="text"
                                startIcon={<Language/>}
                                aria-controls="i18n-menu"
                                aria-haspopup="true"
                                onClick={openMenu}
                            >
                                {labelBySupportedLanguageTag[currentLanguageTag]}
                            </Button>
                            <Menu
                                id="i18n-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={onLanguageChange(null)}
                                PaperProps={{
                                    style: {
                                        maxHeight: ITEM_HEIGHT * 4.5,
                                        width: '20ch',
                                    },
                                }}
                            >
                                {
                                    locale.supported.map(
                                        ({languageTag}) =>
                                            <MenuItem
                                                key={languageTag}
                                                onClick={onLanguageChange(languageTag)}
                                            >
                                                {labelBySupportedLanguageTag[languageTag]}
                                            </MenuItem>
                                    )
                                }
                            </Menu>
                        </Grid>
                    }
                </Grid>

                <Box width='100%'>

                    <Box
                        paddingY='1.5rem'
                        component='header'
                        alignItems='center'
                        display='flex'
                        flexDirection='column'
                        width='100%'
                    >

                        <Box marginBottom={2}>
                            <img width={48} alt='ssegning' src={whale}/>
                        </Box>

                        {
                            !(
                                auth !== undefined &&
                                auth.showUsername &&
                                !auth.showResetCredentials
                            ) ?
                                (
                                    displayRequiredFields ?
                                        (

                                            <div>
                                                <div>
                                                        <span className="subtitle">
                                                            <span className="required">*</span>
                                                            {msg("requiredFields")}
                                                        </span>
                                                </div>
                                                <Typography gutterBottom component="h1" variant="h5"
                                                            id="kc-page-title">{headerNode}</Typography>
                                            </div>

                                        )
                                        :
                                        <Typography gutterBottom component="h1" variant="h5"
                                                    id="kc-page-title">{headerNode}</Typography>
                                ) : (
                                    displayRequiredFields ? (
                                        <div>
                                            <div>
                                            <span className="subtitle"><span
                                                className="required">*</span> {msg("requiredFields")}</span>
                                            </div>
                                            <div className="col-md-10">
                                                {showUsernameNode}
                                                <div>
                                                    <div id="kc-username">
                                                        <label
                                                            id="kc-attempted-username">{auth?.attemptedUsername}</label>
                                                        <Link color='secondary' id="reset-login"
                                                              href={url.loginRestartFlowUrl}>
                                                            <div className="kc-login-tooltip">
                                                                <span
                                                                    className="kc-tooltip-text">{msg("restartLoginTooltip")}</span>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ) : (
                                        <>
                                            {showUsernameNode}
                                            <div>
                                                <div id="kc-username">
                                                    <label id="kc-attempted-username">{auth?.attemptedUsername}</label>
                                                    <Link color='secondary' id="reset-login"
                                                          href={url.loginRestartFlowUrl}>
                                                        <div className="kc-login-tooltip">
                                                            <span
                                                                className="kc-tooltip-text">{msg("restartLoginTooltip")}</span>
                                                        </div>
                                                    </Link>
                                                </div>
                                            </div>
                                        </>
                                    )
                                )
                        }
                    </Box>

                    <CardWrapper>
                        <div id="kc-content">
                            {/* App-initiated actions should not see warning messages about the need to complete the action during login. */}
                            {
                                (
                                    displayMessage &&
                                    message !== undefined &&
                                    (
                                        message.type !== "warning" ||
                                        !isAppInitiatedAction
                                    )
                                ) &&
                                <Alert message={message}/>
                            }
                            <Box padding='1rem'>
                                {formNode}
                            </Box>
                            {
                                (
                                    auth !== undefined &&
                                    auth.showTryAnotherWayLink &&
                                    showAnotherWayIfPresent
                                ) &&

                                <form id="kc-select-try-another-way-form" action={url.loginAction} method="post">
                                    <div>
                                        <div>
                                            <input type="hidden" name="tryAnotherWay" value="on"/>
                                            <Button id="try-another-way"
                                                    onClick={onTryAnotherWayClick}>{msg("doTryAnotherWay")}</Button>
                                        </div>
                                    </div>
                                </form>
                            }
                            {
                                displayInfo &&

                                <div id="kc-info">
                                    <div id="kc-info-wrapper">
                                        {infoNode}
                                    </div>
                                </div>
                            }
                        </div>
                    </CardWrapper>
                </Box>

            </Box>
        </Container>
    );
});
