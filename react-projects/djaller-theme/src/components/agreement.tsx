import {Box, Link, Typography} from "@mui/material";

export function Agreement() {
    return (
        <Box paddingTop='1rem'>
            <Typography variant="body2" color="textSecondary" id="kc-info-wrapper">
                By continuing here you agree to our <Link color='secondary'
                                                          href='https://accounts.ssegning.com/resources/privacy-policy'
                                                          target='_blank' rel='noopener noreferrer'>Privacy
                Policy</Link> and to
                our <Link color='secondary'
                          href='https://accounts.ssegning.com/resources/terms-of-confidentialty'
                          target='_blank' rel='noopener noreferrer'>Terms of
                confidentiality</Link>.
            </Typography>
        </Box>
    );
}
