import React from "react";
import {Box, Typography} from "@mui/material";
import {Copyright} from "@mui/icons-material";

export function CopyrightLine() {
    return (
        <Box display='flex' alignContent='center' justifyContent='center'>
            <Copyright/>
            <Typography marginLeft={1} variant="body2" gutterBottom component="p" color="text.secondary" align="center">
                Made with <Typography color='secondary' component='span'>♥</Typography> since 2021
            </Typography>
        </Box>
    );
}
