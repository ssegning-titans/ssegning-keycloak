import React, {memo, useState} from "react";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {KcContextBase} from "keycloakify";

declare type CredentialType = Pick<KcContextBase.LoginOtp, 'otpLogin'>['otpLogin']['userOtpCredentials'][0];

export const SplitButton = memo(({otpLogin}: Pick<KcContextBase.LoginOtp, 'otpLogin'>) => {
    const [credentialType, setCredentialType] = useState<CredentialType>(otpLogin.userOtpCredentials[0]);
    const credentials = otpLogin.userOtpCredentials;

    const handleSelection = (cred: CredentialType) => {
        setCredentialType(cred);
    };

    return (
        <React.Fragment>
            <input type="hidden" name='selectedCredentialId' value={credentialType.id}/>

            <FormControl fullWidth>
                <InputLabel id="demo-simple-select-helper-label">Select credential</InputLabel>
                <Select
                    labelId="select-credential-label"
                    id="select-credential"
                    label="Select credential"
                    value={credentialType.id}
                    required
                >
                    {credentials.map((option) => (
                        <MenuItem
                            key={option.id}
                            selected={option.id === credentialType.id}
                            onClick={() => handleSelection(option)}
                            value={option.id}
                        >
                            {option.userLabel}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>

        </React.Fragment>
    );
});
