import React from 'react';
import logo from './components/whale.png';
import {Container, Grid, Typography} from "@mui/material";

export default function App() {
    return (
        <Container maxWidth='md'>
            <Grid textAlign='center' marginTop={2}>
                <img width={256} src={logo} className="spp-logo" alt="logo"/>

                <Typography variant="h6" gutterBottom component="p">
                    Keycloak Auth for
                    <Typography
                        color='secondary'
                        variant="h6"
                        marginX={1}
                        component="span"
                    >
                        SSegning
                    </Typography>
                </Typography>

                <Typography variant="body1" gutterBottom component="p">
                    Made with
                    <Typography
                        color='secondary'
                        variant="body1"
                        marginX={1}
                        component="span"
                    >
                        ♥
                    </Typography>
                    by
                    <Typography
                        variant="body1"
                        marginLeft={1}
                        component="a"
                        color='secondary'
                        href="https://ssegning.com"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        ssegning.com
                    </Typography>
                </Typography>
            </Grid>
        </Container>
    );
}
