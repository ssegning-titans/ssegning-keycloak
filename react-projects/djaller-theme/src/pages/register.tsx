import {memo, useMemo, useRef} from "react";
import {I18n, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {KcContext} from "../kc-app/kc-context";
import {Box, Button, Grid, Link, TextField, Typography} from "@mui/material";
import * as Yup from "yup";
import {useFormik} from "formik";
import {Agreement} from "../components/agreement";

type KcContext_Register = Extract<KcContext, { pageId: "register.ftl"; }>;

const RegisterPage = memo(({
                               kcContext,
                               ...props
                           }: { kcContext: KcContext_Register; i18n: I18n; } & KcProps) => {

    const formEl = useRef<HTMLFormElement>(null);
    const {msg, msgStr} = props.i18n;

    const {
        url,
        register,
        realm,
        passwordRequired,
        recaptchaRequired,
        recaptchaSiteKey
    } = kcContext;

    const schema = useMemo(() => Yup.object().shape({
        firstName: Yup.string().required(),
        lastName: Yup.string().required(),
        email: Yup.string().email().required(),
        username: !realm.registrationEmailAsUsername ? Yup.string().required() : null,
        password: passwordRequired ? Yup.string().required() : null,
        'password-confirm': passwordRequired ? Yup.string().required().oneOf([Yup.ref('password'), null], 'Passwords must match') : null,
    }), [realm.registrationEmailAsUsername, passwordRequired]);

    const {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = useFormik({
        initialValues: {
            firstName: register.formData.firstName ?? '',
            lastName: register.formData.lastName ?? '',
            email: register.formData.email ?? '',
            username: register.formData.username ?? '',
            password: '',
            'password-confirm': '',
        },
        validationSchema: schema,
        onSubmit: async (values, {setSubmitting}) => {
            setSubmitting(false);
            if (formEl.current) formEl.current.submit();
        },
    });

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("registerTitle")}
            formNode={
                <Box
                    component='form'
                    onSubmit={handleSubmit}
                    ref={formEl}
                    id="kc-register-form"
                    action={url.registrationAction}
                    method="post"
                >

                    <Grid container spacing={2}>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                fullWidth
                                required
                                label={msg("firstName")}
                                variant="outlined"
                                id="firstName"
                                name="firstName"
                                autoComplete="off"
                                autoFocus
                                helperText={touched.firstName && errors.firstName}
                                disabled={isSubmitting}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.firstName}
                                error={errors.firstName && touched.firstName}
                            />
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <TextField
                                fullWidth
                                required
                                label={msg("lastName")}
                                variant="outlined"
                                id="lastName"
                                name="lastName"
                                autoComplete="off"
                                helperText={touched.lastName && errors.lastName}
                                disabled={isSubmitting}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.lastName}
                                error={errors.lastName && touched.lastName}
                            />
                        </Grid>

                    </Grid>

                    <Box marginY='1rem'>
                        <TextField
                            fullWidth
                            required
                            label={msg("email")}
                            variant="outlined"
                            id="email"
                            name="email"
                            autoComplete="email"
                            type='text'
                            helperText={touched.email && errors.email}
                            disabled={isSubmitting}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                            error={errors.email && touched.email}
                        />
                    </Box>

                    {
                        !realm.registrationEmailAsUsername &&
                        <Box marginY='1rem'>
                            <TextField
                                fullWidth
                                required
                                label={msg("username")}
                                variant="outlined"
                                id="username"
                                name="username"
                                autoComplete="tel"
                                type='text'
                                helperText={touched.username && errors.username}
                                disabled={isSubmitting}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.username}
                                error={errors.username && touched.username}
                            />
                        </Box>
                    }

                    {
                        passwordRequired &&
                        <>
                            <Box marginY='1rem'>
                                <TextField
                                    fullWidth
                                    required
                                    label={msg("password")}
                                    variant="outlined"
                                    id="password"
                                    name="password"
                                    autoComplete="new-password"
                                    type='password'
                                    helperText={touched.password && errors.password}
                                    disabled={isSubmitting}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                    error={errors.password && touched.password}
                                />
                            </Box>
                            <Box marginY='1rem'>
                                <TextField
                                    fullWidth
                                    required
                                    label={msg("passwordConfirm")}
                                    variant="outlined"
                                    id="password-confirm"
                                    name="password-confirm"
                                    autoComplete="new-password"
                                    type='password'
                                    helperText={touched["password-confirm"] && errors["password-confirm"]}
                                    disabled={isSubmitting}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values["password-confirm"]}
                                    error={errors["password-confirm"] && touched["password-confirm"]}
                                />
                            </Box>
                        </>

                    }
                    {
                        recaptchaRequired &&
                        <div className="form-group">
                            <div>
                                <div className="g-recaptcha" data-size="compact" data-sitekey={recaptchaSiteKey}/>
                            </div>
                        </div>
                    }

                    <Agreement/>

                    <div>
                        <Box paddingTop='1rem' id="kc-form-options">
                            <Typography id="kc-registration" variant="body2"
                                        color="textSecondary" align="left">
                                <Link color='secondary' href={url.loginUrl}>
                                    {msg("backToLogin")}
                                </Link>
                            </Typography>
                        </Box>

                        <div id="kc-form-buttons">
                            <Box paddingTop='1rem'>
                                <Button
                                    type='submit'
                                    size="large"
                                    variant="contained"
                                    color="primary"
                                    fullWidth
                                >
                                    {msgStr("doRegister")}
                                </Button>
                            </Box>

                            <input hidden
                                   type="submit"
                                   id='app-register'
                                   value={msgStr("doRegister")}/>
                        </div>
                    </div>
                </Box>
            }
        />
    );
});

export default RegisterPage;
