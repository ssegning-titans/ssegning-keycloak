import {memo} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button, Link, Typography} from "@mui/material";
import {ArrowBack} from "@mui/icons-material";

const ErrorPage = memo(({kcContext, ...props}: { kcContext: KcContextBase.Error; i18n: I18n } & KcProps) => {
    const {message, client} = kcContext;
    const {msg} = props.i18n;

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("errorTitle")}
            displayMessage={false}
            formNode={
                <Box id="kc-error-message">
                    <Typography
                        color='error'
                        variant="body1"
                        gutterBottom
                    >{message.summary}</Typography>
                    {
                        client !== undefined && client.baseUrl !== undefined &&
                        <p>
                            <Button
                                startIcon={<ArrowBack/>}
                                component={Link}
                                id="backToApplication"
                                href={client.baseUrl}
                                size="small"
                            >
                                {msg("backToApplication")}
                            </Button>
                        </p>
                    }
                </Box>
            }
        />
    );
});

export default ErrorPage;
