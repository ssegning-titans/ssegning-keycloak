import {memo, useCallback} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button, Link, TextField, Typography} from "@mui/material";

const LoginResetPasswordPage = memo(({
                                         kcContext,
                                         ...props
                                     }: { kcContext: KcContextBase.LoginResetPassword; i18n: I18n } & KcProps) => {

    const {msg, msgStr} = props.i18n;

    const {
        url,
        realm,
        auth
    } = kcContext;

    const onSubmitClick = useCallback(() => {
        let input = document.getElementById('app-reset-password-submit');
        input.click();
    }, []);

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("emailForgotTitle")}
            formNode={
                <form id="kc-reset-password-form" action={url.loginAction} method="post">
                    <Box marginY='1rem'>
                        <TextField
                            fullWidth
                            required
                            label={
                                !realm.loginWithEmailAllowed ?
                                    msg("username")
                                    :
                                    (
                                        !realm.registrationEmailAsUsername ?
                                            msg("usernameOrEmail") :
                                            msg("email")
                                    )
                            }
                            variant="outlined"
                            id="username"
                            name="username"
                            defaultValue={
                                auth !== undefined && auth.showUsername ?
                                    auth.attemptedUsername : undefined
                            }
                        />
                    </Box>

                    <Box paddingTop='1rem'>
                        <Button size="large"
                                tabIndex={4}
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={onSubmitClick}>Submit</Button>
                    </Box>

                    <Box paddingTop='1rem'>
                        <Typography variant="body2" color="textSecondary" id="kc-info-wrapper">
                            {msg("emailInstruction")}
                        </Typography>
                    </Box>

                    <Box paddingTop='1rem'>
                        <Typography variant='body2'>
                            <Link
                                color='secondary'
                                href={url.loginUrl}
                            >{msg("backToLogin")}</Link>
                        </Typography>

                        <div hidden id="kc-form-buttons">
                            <input
                                id='app-reset-password-submit'
                                type="submit"
                                value={msgStr("doSubmit")}
                            />
                        </div>
                    </Box>
                </form>
            }
        />
    );
});

export default LoginResetPasswordPage;
