import {memo, useCallback} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button, TextField} from "@mui/material";
import {SplitButton} from "../components/credential-selection";

const LoginOtpPage = memo(({kcContext, ...props}: { kcContext: KcContextBase.LoginOtp; i18n: I18n } & KcProps) => {

    const {otpLogin, url} = kcContext;

    const {msg, msgStr} = props.i18n;

    const onSubmitClick = useCallback(() => {
        let input = document.getElementById('kc-login');
        input.click();
    }, []);

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("doLogIn")}
            formNode={

                <form
                    id="kc-otp-login-form"
                    action={url.loginAction}
                    method="post"
                >
                    {
                        otpLogin.userOtpCredentials.length > 1 &&
                        <Box>
                            <SplitButton otpLogin={otpLogin}/>
                        </Box>
                    }

                    <Box marginY='1rem'>
                        <TextField
                            fullWidth
                            required
                            label={msg("loginOtpOneTime")}
                            variant="outlined"
                            id="otp"
                            name="otp"
                            autoComplete="off"
                            type='text'
                            autoFocus
                        />
                    </Box>

                    <Box paddingTop='1rem'>
                        <Button size="large"
                                tabIndex={4}
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={onSubmitClick}>{msgStr("doLogIn")}</Button>
                    </Box>

                    <div hidden>
                        <div id="kc-form-options">
                            <div/>
                        </div>

                        <div id="kc-form-buttons">
                            <input
                                name="login"
                                id="kc-login"
                                type="submit"
                                value={msgStr("doLogIn")}
                            />
                        </div>
                    </div>
                </form>

            }
        />
    );
});

export default LoginOtpPage;
