import {memo, useCallback, useMemo, useRef, useState} from "react";
import {I18n, KcProps} from "keycloakify";
import {Template} from "../components/template";
import * as Yup from "yup";
import {useFormik} from 'formik';
import {KcContext} from "../kc-app/kc-context";
import {Box, Button, Checkbox, Divider, FormControlLabel, Grid, Link, TextField, Typography} from "@mui/material";
import {FacebookOutlined, Google, VpnKeyOutlined} from "@mui/icons-material";
import {blue, red} from "@mui/material/colors";
import {Agreement} from "../components/agreement";

type LocalProp = Extract<KcContext, { pageId: "login-username.ftl"; }>;

const LoginUsernamePage = memo(({kcContext, ...props}: { kcContext: LocalProp; i18n: I18n } & KcProps) => {
    const formEl = useRef<HTMLFormElement>(null);
    const checkBoxEl = useRef<HTMLInputElement>(null);

    const [triedSending, trySending] = useState(false);

    const {msg, msgStr} = props.i18n;

    const {
        social, realm, url,
        usernameEditDisabled, login,
        auth, registrationDisabled
    } = kcContext;

    const schema = useMemo(() => Yup.object().shape({
        username: triedSending ? Yup.string().required() : null,
        rememberMe: Yup.string()
    }), [triedSending]);

    const {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
    } = useFormik({
        initialValues: {
            credentialId: auth?.selectedCredential !== undefined ? auth.selectedCredential : '',
            username: login.username ?? '',
            rememberMe: login.rememberMe!!,
        },
        validationSchema: schema,
        onSubmit: (values, {setSubmitting}) => {
            trySending(true);
            setSubmitting(false);
            if (values.rememberMe && checkBoxEl.current) {
                checkBoxEl.current.value = 'on';
            }
            if (formEl.current && values.username && values.username !== '') formEl.current.submit();
        },
    });

    const shouldSubmit = useCallback(() => {
        if (checkBoxEl.current) {
            checkBoxEl.current.value = 'on';
        }
        let form = document.getElementById('kc-form-login') as HTMLFormElement;
        form.submit();
    }, [checkBoxEl]);

    return (
        <Template
            {...{kcContext, ...props}}
            displayInfo={social.displayInfo}
            displayWide={realm.password && social.providers !== undefined}
            headerNode={msgStr("loginAccountTitle" as any)}
            formNode={
                <Box id="kc-form">
                    <Box id="kc-form-wrapper">
                        {
                            realm.password &&
                            (
                                <form
                                    ref={formEl}
                                    onSubmit={handleSubmit}
                                    id="kc-form-login"
                                    action={url.loginAction}
                                    method="post"
                                >
                                    <Box marginY='1rem'>
                                        <TextField
                                            tabIndex={1}
                                            fullWidth
                                            required
                                            label={
                                                !realm.loginWithEmailAllowed ?
                                                    msg("username")
                                                    :
                                                    (
                                                        !realm.registrationEmailAsUsername ?
                                                            msg("usernameOrEmail") :
                                                            msg("email")
                                                    )
                                            }
                                            variant="outlined"
                                            id="username"
                                            name="username"
                                            type='text'
                                            disabled={usernameEditDisabled}
                                            autoFocus={!usernameEditDisabled}
                                            autoComplete={usernameEditDisabled ? "off" : undefined}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.username}
                                            error={errors.username && touched.username}
                                        />
                                    </Box>

                                    <Box>
                                        <div id="kc-form-options">
                                            {
                                                (
                                                    realm.rememberMe &&
                                                    !usernameEditDisabled
                                                ) &&
                                                <FormControlLabel
                                                    tabIndex={2}
                                                    control={(
                                                        <Checkbox
                                                            inputRef={checkBoxEl}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            value={values.rememberMe}
                                                            id="rememberMe"
                                                            name='rememberMe'
                                                            color="primary"
                                                        />
                                                    )}
                                                    label={msgStr("rememberMe")}
                                                />
                                            }
                                        </div>

                                        <Box paddingTop='1rem'>
                                            <Button size="large"
                                                    tabIndex={3}
                                                    variant="contained"
                                                    color="primary"
                                                    disabled={isSubmitting}
                                                    onClick={shouldSubmit}
                                                    fullWidth>{msgStr("doLogIn")}</Button>
                                        </Box>

                                        <Agreement/>

                                        <Grid paddingTop='1rem' justifyContent='space-between' container>
                                            {
                                                realm.resetPasswordAllowed && (
                                                    <Grid item>
                                                        <Typography variant='body2'>
                                                            <Link color='secondary' href={url.loginResetCredentialsUrl}>
                                                                {msgStr("doForgotPassword")}
                                                            </Link>
                                                        </Typography>
                                                    </Grid>
                                                )
                                            }
                                            {
                                                (
                                                    realm.password &&
                                                    realm.registrationAllowed &&
                                                    !registrationDisabled
                                                ) && (
                                                    <Grid item>
                                                        <Typography variant='body2'>
                                                            <Link color='secondary' href={url.registrationUrl}>
                                                                {msgStr("noAccount")}
                                                                {' '}
                                                                {msgStr("doRegister")}
                                                            </Link>
                                                        </Typography>
                                                    </Grid>
                                                )
                                            }
                                        </Grid>

                                    </Box>

                                    <div hidden id="kc-form-buttons">
                                        <input
                                            type="hidden"
                                            id="id-hidden-input"
                                            name="credentialId"
                                            value={values.credentialId}
                                        />
                                        <input
                                            type="hidden"
                                            id="kc-login"
                                            name="login"
                                            value='Submit'
                                        />
                                    </div>
                                </form>
                            )
                        }
                    </Box>

                    {
                        (realm.password && social.providers !== undefined) &&
                        <>
                            <Box marginY='1.5rem'>
                                <Divider/>
                            </Box>
                            <Box id="kc-social-providers">
                                {
                                    social.providers.map(p => (
                                        <Box marginY='1rem' key={p.providerId}>
                                            <Button variant="outlined"
                                                    startIcon={
                                                        p.providerId === 'google'
                                                            ? <Google sx={{color: red[500]}}/>
                                                            : p.providerId === 'facebook'
                                                                ? <FacebookOutlined sx={{color: blue[500]}}/>
                                                                : p.providerId === 'keycloak'
                                                                    ? <VpnKeyOutlined sx={{color: blue[700]}}/>
                                                                    : null
                                                    }
                                                    fullWidth
                                                    component={Link}
                                                    href={p.loginUrl}>
                                                {p.displayName}
                                            </Button>
                                        </Box>
                                    ))
                                }
                            </Box>
                        </>
                    }
                </Box>
            }
        />
    );
});

export default LoginUsernamePage;
