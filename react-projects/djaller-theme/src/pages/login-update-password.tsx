import {memo, useCallback, useMemo, useRef, useState} from "react";
import {I18n, KcProps} from "keycloakify";
import {Template} from "../components/template";
import * as Yup from "yup";
import {useFormik} from 'formik';
import {KcContext} from "../kc-app/kc-context";
import {Box, Button, Checkbox, FormControlLabel, Stack, TextField} from "@mui/material";
import {Agreement} from "../components/agreement";

type LocalProp = Extract<KcContext, { pageId: "login-update-password.ftl"; }>;

const LoginUpdatePasswordPage = memo(({kcContext, ...props}: { kcContext: LocalProp; i18n: I18n } & KcProps) => {
    const formEl = useRef<HTMLFormElement>(null);
    const checkBoxEl = useRef<HTMLInputElement>(null);
    const cancelInputEl = useRef<HTMLInputElement>(null);

    const [triedSending, trySending] = useState(false);

    const {msg, msgStr} = props.i18n;

    const {url, isAppInitiatedAction, username} = kcContext;

    const schema = useMemo(() => Yup.object().shape({
        password: triedSending ? Yup.string().required('Password is required') : null,
        'password-new': triedSending ? Yup.string().required('Password is required') : null,
        'password-confirm': triedSending ? Yup.string().oneOf([Yup.ref('password-new'), null], 'Passwords must match') : null,
        'logout-sessions': Yup.string().required(),
    }), [triedSending]);

    const {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
    } = useFormik({
        initialValues: {
            password: '',
            'password-new': '',
            'password-confirm': '',
            'logout-sessions': 'on',
        },
        validationSchema: schema,
        onSubmit: (values, {setSubmitting}) => {
            trySending(true);
            setSubmitting(false);
            if (values['logout-sessions'] && checkBoxEl.current) {
                checkBoxEl.current.value = 'on';
            }
            if (formEl.current && values.password && values.password !== '' && values['password-confirm'] !== '' && values['password-new'] !== '') formEl.current.submit();
        },
    });

    const shouldSubmit = useCallback(() => {
        if (checkBoxEl.current) {
            checkBoxEl.current.value = 'on';
        }
        let form = document.getElementById('kc-form-login') as HTMLFormElement;
        form.submit();
    }, [checkBoxEl]);

    const shouldCancel = useCallback(() => {
        if (cancelInputEl.current) {
            cancelInputEl.current.click();
        }
    }, [cancelInputEl]);

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msgStr("updatePasswordTitle" as any)}
            formNode={
                <Box id="kc-form">
                    <Box id="kc-form-wrapper">
                        <form
                            ref={formEl}
                            onSubmit={handleSubmit}
                            id="kc-form-login"
                            action={url.loginAction}
                            method="post"
                        >
                            <Box marginY='1rem'>
                                <TextField
                                    tabIndex={1}
                                    fullWidth
                                    required
                                    label={msg("password")}
                                    variant="outlined"
                                    id="password"
                                    name="password"
                                    type='password'
                                    disabled={isSubmitting}
                                    autoFocus
                                    autoComplete='current-password'
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                    error={errors.password && touched.password}
                                />
                            </Box>

                            <Box marginY='1rem'>
                                <TextField
                                    tabIndex={1}
                                    fullWidth
                                    required
                                    label={msg("passwordNew")}
                                    variant="outlined"
                                    id="password-new"
                                    name="password-new"
                                    type='password'
                                    disabled={isSubmitting}
                                    autoComplete='password-new'
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values['password-new']}
                                    error={errors['password-new'] && touched['password-new']}
                                />
                            </Box>
                            <Box marginY='1rem'>
                                <TextField
                                    tabIndex={1}
                                    fullWidth
                                    required
                                    label={msg("passwordConfirm")}
                                    variant="outlined"
                                    id="password-confirm"
                                    name="password-confirm"
                                    type='password'
                                    disabled={isSubmitting}
                                    autoComplete='password-confirm'
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values['password-confirm']}
                                    error={errors['password-confirm'] && touched['password-confirm']}
                                />
                            </Box>

                            <Box>
                                <div id="kc-form-options">
                                    {
                                        isAppInitiatedAction &&
                                        <FormControlLabel
                                            tabIndex={2}
                                            control={(
                                                <Checkbox
                                                    inputRef={checkBoxEl}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values['logout-sessions']}
                                                    id="logout-sessions"
                                                    name='logout-sessions'
                                                    color="primary"
                                                />
                                            )}
                                            label={msgStr("logoutOtherSessions")}
                                        />
                                    }
                                </div>

                                <Box paddingTop='1rem'>
                                    <Stack direction="column" spacing={2}>

                                        <Button
                                            size="large"
                                            variant="contained"
                                            color="primary"
                                            disabled={isSubmitting}
                                            onClick={shouldSubmit}
                                        >{msgStr("doLogIn")}</Button>

                                        {isAppInitiatedAction && (
                                            <Button
                                                size="large"
                                                variant="text"
                                                color="primary"
                                                disabled={isSubmitting}
                                                onClick={shouldCancel}
                                            >{msgStr("doCancel")}</Button>
                                        )}

                                    </Stack>
                                </Box>

                                <Agreement/>

                            </Box>

                            <div hidden id="kc-form-buttons">
                                <input
                                    type="text"
                                    name="username"
                                    value={username}
                                    autoComplete="username"
                                    readOnly
                                />
                                <input
                                    type="submit"
                                    name="login"
                                    value={msgStr("doSubmit")}
                                />
                            </div>
                        </form>

                        <form
                            hidden
                            action={url.loginAction}
                            method="post"
                        >
                            <input
                                type="text"
                                name="username"
                                value={username}
                                autoComplete="username"
                                readOnly
                            />
                            <input
                                ref={cancelInputEl}
                                type="submit"
                                name="cancel-aia"
                                value={msgStr("doCancel")}
                            />
                        </form>
                    </Box>
                </Box>
            }
        />
    );
});

export default LoginUpdatePasswordPage;
