import {memo, useCallback} from "react";
import {I18n, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {KcContext} from "../kc-app/kc-context";
import {Box, Button} from "@mui/material";

type KcContext_Terms = Extract<KcContext, { pageId: "terms.ftl"; }>;

const TermsPage = memo(({kcContext, ...props}: { kcContext: KcContext_Terms; i18n: I18n; } & KcProps) => {

    const {msg, msgStr} = props.i18n;

    const {url} = kcContext;

    const onSubmitClick = useCallback(() => {
        let input = document.getElementById('kc-accept');
        input.click();
    }, []);

    const onCancelClick = useCallback(() => {
        let input = document.getElementById('kc-decline');
        input.click();
    }, []);

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("termsTitle")}
            formNode={
                <>
                    <Box id="kc-terms-text">
                        {msg("termsText")}
                    </Box>
                    <form className="form-actions" action={url.loginAction} method="POST">
                        <Box paddingTop='1rem'>
                            <Button size="large"
                                    variant="contained"
                                    color="primary"
                                    fullWidth
                                    onClick={onSubmitClick}>{msgStr("doAccept")}</Button>
                        </Box>
                        <Box paddingTop='1rem'>
                            <Button size="large"
                                    variant="text"
                                    color="error"
                                    fullWidth
                                    onClick={onCancelClick}>{msgStr("doDecline")}</Button>
                        </Box>

                        <input
                            hidden
                            name="accept"
                            id="kc-accept"
                            type="submit"
                        />
                        <input
                            hidden
                            name="cancel"
                            id="kc-decline"
                            type="submit"
                        />
                    </form>
                    <div className="clearfix"/>
                </>
            }
        />
    );
});

export default TermsPage;
