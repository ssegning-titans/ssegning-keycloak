import {memo} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button, TextField} from "@mui/material";

const LoginUpdateProfilePage = memo(({
                                         kcContext,
                                         ...props
                                     }: { kcContext: KcContextBase.LoginUpdateProfile; i18n: I18n } & KcProps) => {

    const {msg, msgStr} = props.i18n;

    const {url, user, isAppInitiatedAction} = kcContext;

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("loginProfileTitle")}
            formNode={
                <form id="kc-update-profile-form" action={url.loginAction}
                      method="post">
                    {user.editUsernameAllowed && (
                        <Box marginY='1rem'>
                            <TextField
                                fullWidth
                                required
                                defaultValue={user.username ?? ""}
                                label={msg("username")}
                                variant="outlined"
                                id="username"
                                name="username"
                                type="text"
                            />
                        </Box>
                    )}

                    <Box marginY='1rem'>
                        <TextField
                            fullWidth
                            required
                            defaultValue={user.email ?? ""}
                            label={msg("email")}
                            variant="outlined"
                            id="email"
                            name="email"
                            type="email"
                        />
                    </Box>

                    <Box marginY='1rem'>
                        <TextField
                            fullWidth
                            required
                            defaultValue={user.firstName ?? ""}
                            label={msg("firstName")}
                            variant="outlined"
                            id="firstName"
                            name="firstName"
                            type="text"
                        />
                    </Box>

                    <Box marginY='1rem'>
                        <TextField
                            fullWidth
                            required
                            defaultValue={user.lastName ?? ""}
                            label={msg("lastName")}
                            variant="outlined"
                            id="lastName"
                            name="lastName"
                            type="text"
                        />
                    </Box>

                    <Box paddingTop='1rem'>
                        <Button
                            size="large"
                            variant="contained"
                            color="primary"
                            fullWidth
                            name='submit'
                            type="submit"
                            defaultValue={msgStr("doSubmit")}>
                            {msgStr("doSubmit")}
                        </Button>
                    </Box>
                    {
                        isAppInitiatedAction &&
                        <Box paddingTop='1rem'>
                            <Button
                                size="large"
                                variant="text"
                                color="error"
                                fullWidth
                                id='app-cancel'
                                type="submit"
                                name="cancel-aia"
                                value="true"
                            >
                                {msgStr("doDecline")}
                            </Button>
                        </Box>
                    }

                    <div hidden>
                        <div id="kc-form-options">
                            <div/>
                        </div>
                    </div>

                </form>
            }
        />
    );

});

export default LoginUpdateProfilePage;
