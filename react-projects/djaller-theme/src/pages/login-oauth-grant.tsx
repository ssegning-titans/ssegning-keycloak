import {memo} from "react";
import {I18n, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button, Divider, List, ListItem, ListItemIcon, ListItemText, Typography} from "@mui/material";
import {ArrowForwardIos} from "@mui/icons-material";
import {KcContext} from "../kc-app/kc-context";

type LocalKcContext = Extract<KcContext, { pageId: "login-oauth-grant.ftl"; }>;

const LoginOauthGrantPage = memo(({kcContext, ...props}: { kcContext: LocalKcContext; i18n: I18n } & KcProps) => {

    const {
        url, client, oauth
    } = kcContext;
    const {msg, msgStr} = props.i18n;

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msgStr("oauthGrantTitle", client.name ?? client.clientId)}
            formNode={
                <Box id="instruction1">
                    <Typography gutterBottom component="p" variant="h5">
                        {msg("oauthGrantRequest")}
                    </Typography>
                    <List>
                        {
                            oauth.clientScopesRequested.map((scope) => (
                                <ListItem key={scope.consentScreenText} id={'scope-grated-' + scope.consentScreenText}>
                                    <ListItemIcon>
                                        <ArrowForwardIos/>
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={scope.consentScreenText}
                                        secondary={scope.description}
                                    />
                                </ListItem>
                            ))
                        }
                        <Divider/>
                    </List>

                    <form className="form-actions" action={url.oauthAction} method="POST">
                        <input type="hidden" name="code" value={oauth.code}/>

                        <Box paddingTop='1rem'>
                            <Button size="large"
                                    name="accept" id="kc-login" type="submit" value={msgStr("doYes")}
                                    variant="contained"
                                    color="primary"
                                    fullWidth>{msgStr("doYes")}</Button>
                        </Box>
                        <Box paddingTop='1rem'>
                            <Button size="large"
                                    name="cancel" id="kc-cancel" type="submit" value={msgStr("doNo")}
                                    variant="outlined"
                                    color="error"
                                    fullWidth>{msgStr("doNo")}</Button>
                        </Box>
                    </form>

                </Box>
            }
        />
    );
});

export default LoginOauthGrantPage;
