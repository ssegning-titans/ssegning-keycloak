import {memo, useCallback, useMemo, useRef, useState} from "react";
import {I18n, KcProps} from "keycloakify";
import {Template} from "../components/template";
import * as Yup from "yup";
import {useFormik} from 'formik';
import {KcContext} from "../kc-app/kc-context";
import {Box, Button, Grid, Link, TextField, Typography} from "@mui/material";
import {Agreement} from "../components/agreement";

type LocalProp = Extract<KcContext, { pageId: "login-password.ftl"; }>;

const LoginPasswordPage = memo(({kcContext, ...props}: { kcContext: LocalProp; i18n: I18n } & KcProps) => {
    const formEl = useRef<HTMLFormElement>(null);
    const checkBoxEl = useRef<HTMLInputElement>(null);

    const [triedSending, trySending] = useState(false);

    const {msg, msgStr} = props.i18n;

    const {
        realm, url,
    } = kcContext;

    const schema = useMemo(() => Yup.object().shape({
        password: triedSending ? Yup.string().required() : null,
    }), [triedSending]);

    const {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
    } = useFormik({
        initialValues: {
            password: '',
        },
        validationSchema: schema,
        onSubmit: (values, {setSubmitting}) => {
            trySending(true);
            setSubmitting(false);
            if (values.password && checkBoxEl.current) {
                checkBoxEl.current.value = 'on';
            }
            if (formEl.current && values.password && values.password !== '') formEl.current.submit();
        },
    });

    const shouldSubmit = useCallback(() => {
        if (checkBoxEl.current) {
            checkBoxEl.current.value = 'on';
        }
        let form = document.getElementById('kc-form-login') as HTMLFormElement;
        form.submit();
    }, [checkBoxEl]);

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msgStr("password")}
            formNode={
                <Box id="kc-form">
                    <Box id="kc-form-wrapper">
                        <form
                            ref={formEl}
                            onSubmit={handleSubmit}
                            id="kc-form-login"
                            action={url.loginAction}
                            method="post"
                        >
                            <Box marginY='1rem'>
                                <TextField
                                    tabIndex={1}
                                    fullWidth
                                    required
                                    label={msg("password")}
                                    variant="outlined"
                                    id="password"
                                    name="password"
                                    type='password'
                                    disabled={isSubmitting}
                                    autoFocus
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                    error={errors.password && touched.password}
                                />
                            </Box>

                            <Box>

                                <Box paddingTop='1rem'>
                                    <Button size="large"
                                            tabIndex={3}
                                            variant="contained"
                                            color="primary"
                                            disabled={isSubmitting}
                                            onClick={shouldSubmit}
                                            fullWidth>{msgStr("doLogIn")}</Button>
                                </Box>

                                <Agreement/>

                                <Grid paddingTop='1rem' justifyContent='space-between' container>
                                    {
                                        realm.resetPasswordAllowed && (
                                            <Grid item>
                                                <Typography variant='body2'>
                                                    <Link color='secondary' href={url.loginResetCredentialsUrl}>
                                                        {msgStr("doForgotPassword")}
                                                    </Link>
                                                </Typography>
                                            </Grid>
                                        )
                                    }
                                </Grid>

                            </Box>

                            <div hidden id="kc-form-buttons">
                                <input
                                    type="hidden"
                                    id="kc-login"
                                    name="login"
                                    value='Submit'
                                />
                            </div>
                        </form>
                    </Box>
                </Box>
            }
        />
    );
});

export default LoginPasswordPage;
