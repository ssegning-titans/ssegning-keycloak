import {memo} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button, Link, Typography} from "@mui/material";
import {ArrowBack, ArrowForward} from "@mui/icons-material";

const InfoApp = memo(({kcContext, ...props}: { kcContext: KcContextBase.Info; i18n: I18n } & KcProps) => {

    const {msg} = props.i18n;

    const {
        messageHeader,
        message,
        requiredActions,
        skipLink,
        pageRedirectUri,
        actionUri,
        client
    } = kcContext;

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={
                messageHeader !== undefined ?
                    <>{messageHeader}</>
                    :
                    <>{message.summary}</>
            }
            displayMessage={false}
            formNode={
                <Box id="kc-info-message">
                    <Box>
                        <Typography variant='body1' color='success'>
                            {message.summary}
                        </Typography>

                        {
                            requiredActions !== undefined &&
                            <b>
                                {
                                    requiredActions
                                        .map(requiredAction => msg(`requiredAction.${requiredAction}` as const))
                                        .join(",")
                                }

                            </b>

                        }
                    </Box>

                    <Box component='p'>
                        {
                            !skipLink &&
                            pageRedirectUri !== undefined ?
                                <Button startIcon={<ArrowBack/>}
                                        component={Link}
                                        href={pageRedirectUri}>{(msg("backToApplication"))}</Button>
                                :
                                actionUri !== undefined ?
                                    <Button endIcon={<ArrowForward/>}
                                            component={Link}
                                            href={actionUri}>{msg("proceedWithAction")}</Button>
                                    :
                                    client.baseUrl !== undefined &&
                                    <Button startIcon={<ArrowBack/>}
                                            component={Link}
                                            href={client.baseUrl}>{msg("backToApplication")}</Button>
                        }
                    </Box>
                </Box>


            }
        />
    );
});

export default InfoApp;

