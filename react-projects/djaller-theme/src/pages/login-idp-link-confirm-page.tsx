import {memo} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button} from "@mui/material";
import {ArrowBack, ArrowForward} from "@mui/icons-material";

const LoginIdpLinkConfirmPage = memo(({
                                          kcContext,
                                          ...props
                                      }: { kcContext: KcContextBase.LoginIdpLinkConfirm; i18n: I18n } & KcProps) => {

    const {msg} = props.i18n;

    const {url, idpAlias} = kcContext;

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("confirmLinkIdpTitle")}
            formNode={
                <form id="kc-register-form" action={url.loginAction} method="post">

                    <Box paddingTop='1rem'>
                        <Button size="large"
                                endIcon={<ArrowForward/>}
                                name="submitAction"
                                id="updateProfile"
                                value="updateProfile"
                                variant="contained"
                                fullWidth
                                type='submit'>{msg("confirmLinkIdpReviewProfile")}</Button>
                    </Box>

                    <Box paddingTop='1rem'>
                        <Button size="large"
                                startIcon={<ArrowBack/>}
                                name="submitAction"
                                id="linkAccount"
                                value="linkAccount"
                                variant="outlined"
                                fullWidth
                                type='submit'>{msg("confirmLinkIdpContinue", idpAlias)}</Button>
                    </Box>

                </form>
            }
        />
    );

});

export default LoginIdpLinkConfirmPage;
