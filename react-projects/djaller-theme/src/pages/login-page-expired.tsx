import {memo} from "react";
import {I18n, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Divider, List, ListItem, ListItemButton, ListItemIcon, ListItemText} from "@mui/material";
import {KcContext} from "../kc-app/kc-context";
import {FastForward, RestartAlt} from "@mui/icons-material";

type LocalKcContext = Extract<KcContext, { pageId: "login-page-expired.ftl"; }>;

const LoginPageExpiredPage = memo(({kcContext, ...props}: { kcContext: LocalKcContext; i18n: I18n } & KcProps) => {

    const {
        url
    } = kcContext;
    const {msg} = props.i18n;

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg('pageExpiredTitle')}
            formNode={
                <Box id="instruction1">
                    <List>
                        <ListItem>
                            <ListItemButton component="a" href={url.loginRestartFlowUrl}>
                                <ListItemIcon>
                                    <RestartAlt/>
                                </ListItemIcon>
                                <ListItemText primary={msg("pageExpiredMsg1")}/>
                            </ListItemButton>
                        </ListItem>
                        <Divider/>
                        <ListItem>
                            <ListItemButton component="a" href={url.loginAction}>
                                <ListItemIcon>
                                    <FastForward/>
                                </ListItemIcon>
                                <ListItemText primary={msg("pageExpiredMsg2")}/>
                            </ListItemButton>
                        </ListItem>
                    </List>

                </Box>
            }
        />
    );
});

export default LoginPageExpiredPage;

