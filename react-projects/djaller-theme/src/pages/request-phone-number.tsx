import * as Yup from "yup";
import {KcContext} from "../kc-app/kc-context";
import {memo, useRef} from "react";
import {I18n, KcProps} from "keycloakify";
import {useFormik} from "formik";
import {Template} from "../components/template";
import {Box, Button, TextField, Typography} from "@mui/material";

const schema = Yup.object().shape({
    phoneNumber: Yup.string().required(),
});

type LocalProp = Extract<KcContext, { pageId: "request-phone-number.ftl"; }>;

const RequestPhoneNumber = memo(({kcContext, ...props}: { kcContext: LocalProp; i18n: I18n } & KcProps) => {
    const formEl = useRef<HTMLFormElement>(null);
    const {url} = kcContext;

    const {msg, msgStr} = props.i18n;
    const tProps: any = {...{kcContext, ...props}};

    const {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
    } = useFormik({
        initialValues: {
            phoneNumber: '',
            login: msgStr("doLogIn"),
        },
        validationSchema: schema,
        onSubmit: (values, {setSubmitting}) => {
            setSubmitting(false);
            if (formEl.current) formEl.current.submit();
        },
    });

    return (
        <Template
            {...tProps}
            headerNode={msg("doLogIn")}
            formNode={
                <>
                    <form
                        ref={formEl}
                        id="kc-phone-number-login-form"
                        action={url.loginAction}
                        method="post"
                        onSubmit={handleSubmit}
                    >

                        <Typography variant="h6" component="p">
                            Please provide your phone number
                        </Typography>
                        <Typography variant='body1' gutterBottom component='p'>
                            We'll send you a Tan to verify your identity
                        </Typography>

                        <Box marginY='1rem'>
                            <TextField
                                fullWidth
                                label="Phone number"
                                variant="outlined"
                                id="phoneNumber"
                                name="phoneNumber"
                                autoComplete="tel"
                                type="string"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.phoneNumber}
                                error={errors.phoneNumber && touched.phoneNumber}
                            />
                        </Box>

                        <Typography variant="body1" gutterBottom component="p">
                            Ready your phone
                        </Typography>

                        <Box paddingTop='1rem'>
                            <Button
                                size="large"
                                variant="contained"
                                color="primary"
                                fullWidth
                                disabled={isSubmitting}
                                type='submit'
                            >
                                Submit
                            </Button>
                        </Box>

                        <div hidden>
                            <div id="kc-form-options">
                                <div/>
                            </div>

                            <input
                                name="login"
                                id="kc-submit"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.login}
                            />
                        </div>
                    </form>
                </>
            }
        />
    );

});

export default RequestPhoneNumber;
