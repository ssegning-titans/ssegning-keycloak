import {memo, useRef} from "react";
import type {I18n, KcProps} from "keycloakify";
import type {KcContext} from "../kc-app/kc-context";
import {Box, Button, InputAdornment, TextField, Typography} from "@mui/material";
import {Template} from "../components/template";
import {useFormik} from "formik";
import * as Yup from "yup";

const schema = Yup.object().shape({
    smsTan: Yup.number().required(),
});

type LocalProp = Extract<KcContext, { pageId: "request-sms-tan.ftl"; }>;

const RequestSmsTan = memo(({kcContext, ...props}: { kcContext: LocalProp; i18n: I18n } & KcProps) => {
    const formEl = useRef<HTMLFormElement>(null);
    const {phoneNumber, url} = kcContext;

    const {msg, msgStr} = props.i18n;
    const tProps: any = {...{kcContext, ...props}};

    const {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
    } = useFormik({
        initialValues: {
            smsTan: '',
            login: msgStr("doLogIn"),
        },
        validationSchema: schema,
        onSubmit: (values, {setSubmitting}) => {
            setSubmitting(false);
            if (formEl.current) formEl.current.submit();
        },
    });

    return (
        <Template
            {...tProps}
            headerNode={msg("doLogIn")}
            formNode={

                <>
                    <form
                        ref={formEl}
                        id="kc-sms-tan-login-form"
                        action={url.loginAction}
                        method="post"
                        onSubmit={handleSubmit}
                    >

                        <Typography variant="h6" gutterBottom component="p">
                            We've send an SMS to <b>{phoneNumber}</b>
                        </Typography>

                        <Box marginY='1rem'>
                            <TextField
                                fullWidth
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">T-</InputAdornment>,
                                }}
                                label="Sms tan"
                                variant="outlined"
                                id="smsTan"
                                name="smsTan"
                                autoComplete="off"
                                type="number"
                                helperText={'Check your phone for new SMS'}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.smsTan}
                                error={errors.smsTan && touched.smsTan}
                            />
                        </Box>

                        <Typography variant="body1" gutterBottom component="p">
                            Check your phone for new SMS
                        </Typography>

                        <Box paddingTop='1rem'>
                            <Button
                                size="large"
                                variant="contained"
                                color="primary"
                                fullWidth
                                disabled={isSubmitting}
                                type='submit'
                            >
                                Submit
                            </Button>
                        </Box>

                        <div hidden>
                            <div id="kc-form-options">
                                <div/>
                            </div>

                            <input
                                name="login"
                                id="kc-submit"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.login}
                            />
                        </div>
                    </form>
                    <Box
                        component='form'
                        action={url.loginAction}
                        method="post"
                        width='100%'
                    >
                        <Box paddingTop='0.75rem'>
                            <Button
                                size="large"
                                variant="outlined"
                                color="primary"
                                fullWidth

                                name="resend"
                                id="app-resend-tan"
                                type="submit"
                                value='reSendMobileTan'
                            >
                                Resend tan
                            </Button>
                        </Box>
                    </Box>
                </>

            }
        />
    );

});

export default RequestSmsTan;
