import {memo} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Link, Typography} from "@mui/material";

const LoginVerifyEmailPage = memo(({
                                       kcContext,
                                       ...props
                                   }: { kcContext: KcContextBase.LoginVerifyEmail; i18n: I18n } & KcProps) => {

    const {msg} = props.i18n;

    const {
        url
    } = kcContext;

    return (
        <Template
            {...{kcContext, ...props}}
            headerNode={msg("emailVerifyTitle")}
            formNode={
                <>
                    <Typography gutterBottom component="p" variant="h5">
                        {msg("emailVerifyInstruction1")}
                    </Typography>
                    <Box paddingTop='1rem'>
                        <Typography variant="body2" color="textSecondary">
                            {msg("emailVerifyInstruction2")}
                            <Box component='span' marginX='0.25rem'>
                                <Link color='secondary' href={url.loginAction}>{msg("doClickHere")}</Link>
                            </Box>
                            {msg("emailVerifyInstruction3")}
                        </Typography>
                    </Box>
                </>
            }
        />
    );

});

export default LoginVerifyEmailPage;
