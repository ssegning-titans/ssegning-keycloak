import {memo, useCallback, useRef, useState} from "react";
import {I18n, KcContextBase, KcProps} from "keycloakify";
import {Template} from "../components/template";
import {Box, Button, Checkbox, Divider, FormControlLabel, Grid, Link, TextField, Typography} from "@mui/material";

const LoginPage = memo(({kcContext, ...props}: { kcContext: KcContextBase.Login; i18n: I18n } & KcProps) => {
    const checkBoxEl = useRef<HTMLInputElement>(null);

    const {msg, msgStr} = props.i18n;

    const {
        social, realm, url,
        usernameEditDisabled, login,
        auth, registrationDisabled
    } = kcContext;

    const [isLoginButtonDisabled, setIsLoginButtonDisabled] = useState(false);

    const [rememberMe, setRememberMe] = useState(login.rememberMe);

    const shouldSubmit = useCallback(() => {
        setIsLoginButtonDisabled(true);
        if (rememberMe && checkBoxEl.current) {
            checkBoxEl.current.value = 'on';
        }
        let form = document.getElementById('kc-form-login') as HTMLFormElement;
        form.submit();
    }, [rememberMe, checkBoxEl]);


    return (
        <Template
            {...{kcContext, ...props}}
            displayInfo={social.displayInfo}
            displayWide={realm.password && social.providers !== undefined}
            headerNode={msg("loginAccountTitle" as any)}
            formNode={
                <Box id="kc-form">
                    <Box id="kc-form-wrapper">
                        {
                            realm.password &&
                            (
                                <form id="kc-form-login" action={url.loginAction} method="post">
                                    <Box marginY='1rem'>
                                        <TextField
                                            fullWidth
                                            required
                                            label={
                                                !realm.loginWithEmailAllowed ?
                                                    msg("username")
                                                    :
                                                    (
                                                        !realm.registrationEmailAsUsername ?
                                                            msg("usernameOrEmail") :
                                                            msg("email")
                                                    )
                                            }
                                            variant="outlined"
                                            id="username"
                                            name="username"
                                            type={realm.loginWithEmailAllowed && realm.registrationEmailAsUsername ? 'email' : 'text'}
                                            defaultValue={login.username ?? ''}
                                            {...(usernameEditDisabled ? {"disabled": true} : {
                                                "autoFocus": true,
                                                "autoComplete": "off"
                                            })}
                                        />
                                    </Box>

                                    <Box marginY='1rem'>
                                        <TextField
                                            fullWidth
                                            required
                                            label={msg("password")}
                                            variant="outlined"
                                            tabIndex={2}
                                            id="password"
                                            name="password"
                                            type="password"
                                            autoComplete="off"
                                        />
                                    </Box>

                                    <Box>
                                        <div id="kc-form-options">
                                            {
                                                (
                                                    realm.rememberMe &&
                                                    !usernameEditDisabled
                                                ) &&
                                                <FormControlLabel
                                                    tabIndex={3}
                                                    control={(
                                                        <Checkbox
                                                            inputRef={checkBoxEl}
                                                            checked={rememberMe}
                                                            id="rememberMe"
                                                            name='rememberMe'
                                                            color="primary"
                                                            onChange={() => setRememberMe(prev => !prev)}
                                                        />
                                                    )}
                                                    label={msgStr("rememberMe")}
                                                />
                                            }
                                        </div>

                                        <Box paddingTop='1rem'>
                                            <Button size="large"
                                                    tabIndex={4}
                                                    name="login" id="kc-login" type="submit" value='Submit'
                                                    disabled={isLoginButtonDisabled}
                                                    variant="contained"
                                                    color="primary"
                                                    onClick={shouldSubmit}
                                                    fullWidth>{msgStr("doLogIn")}</Button>
                                        </Box>

                                        {
                                            (
                                                realm.password &&
                                                realm.registrationAllowed &&
                                                !registrationDisabled
                                            ) &&
                                            <Box paddingTop='1rem'>
                                                <Button size="large"
                                                        href={url.registrationUrl}
                                                        tabIndex={5}
                                                        id="kc-register" type="button"
                                                        disabled={isLoginButtonDisabled}
                                                        variant="outlined"
                                                        color="primary"
                                                        fullWidth>{msg("doRegister")}</Button>
                                            </Box>
                                        }

                                        <Grid paddingTop='1rem' justifyContent='space-between' container>
                                            <Grid item>
                                                {
                                                    realm.resetPasswordAllowed &&
                                                    <Typography variant='body2'>
                                                        <Link
                                                            color='secondary'
                                                            tabIndex={6}
                                                            href={url.loginResetCredentialsUrl}
                                                        >{msg("doForgotPassword")}</Link>
                                                    </Typography>
                                                }
                                            </Grid>
                                        </Grid>

                                    </Box>

                                    <div hidden id="kc-form-buttons">
                                        <input
                                            type="hidden"
                                            id="id-hidden-input"
                                            name="credentialId"
                                            {...(auth?.selectedCredential !== undefined ? {"value": auth.selectedCredential} : {})}
                                        />
                                    </div>
                                </form>
                            )
                        }
                    </Box>

                    {
                        (realm.password && social.providers !== undefined) &&
                        <>
                            <Box marginY='1.5rem'>
                                <Divider/>
                            </Box>
                            <Box id="kc-social-providers">
                                {
                                    social.providers.map(p =>
                                        <Button variant="outlined"
                                                key={p.providerId}
                                                fullWidth
                                                component={Link}
                                                href={p.loginUrl}>
                                            {p.displayName}
                                        </Button>
                                    )
                                }
                            </Box>
                        </>
                    }
                </Box>
            }
        />
    );
});

export default LoginPage;
